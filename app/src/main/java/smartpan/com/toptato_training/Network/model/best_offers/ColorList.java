package smartpan.com.toptato_training.Network.model.best_offers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ColorList {

    @SerializedName("ColorId")
    @Expose
    private long colorId;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;

    /**
     * No args constructor for use in serialization
     *
     */
    public ColorList() {
    }

    /**
     *
     * @param arabicName
     * @param englishName
     * @param colorId
     */
    public ColorList(long colorId, String arabicName, String englishName) {
        super();
        this.colorId = colorId;
        this.arabicName = arabicName;
        this.englishName = englishName;
    }

    public long getColorId() {
        return colorId;
    }

    public void setColorId(long colorId) {
        this.colorId = colorId;
    }

    public ColorList withColorId(long colorId) {
        this.colorId = colorId;
        return this;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public ColorList withArabicName(String arabicName) {
        this.arabicName = arabicName;
        return this;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public ColorList withEnglishName(String englishName) {
        this.englishName = englishName;
        return this;
    }

}