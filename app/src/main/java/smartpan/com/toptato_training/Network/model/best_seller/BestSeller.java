package smartpan.com.toptato_training.Network.model.best_seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BestSeller {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("BestSeller")
    @Expose
    private List<BestSeller_> bestSeller = new ArrayList<BestSeller_>();

    /**
     * No args constructor for use in serialization
     *
     */
    public BestSeller() {
    }

    /**
     *
     * @param success
     * @param bestSeller
     */
    public BestSeller(String success, List<BestSeller_> bestSeller) {
        super();
        this.success = success;
        this.bestSeller = bestSeller;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public BestSeller withSuccess(String success) {
        this.success = success;
        return this;
    }

    public List<BestSeller_> getBestSeller() {
        return bestSeller;
    }

    public void setBestSeller(List<BestSeller_> bestSeller) {
        this.bestSeller = bestSeller;
    }

    public BestSeller withBestSeller(List<BestSeller_> bestSeller) {
        this.bestSeller = bestSeller;
        return this;
    }
}
