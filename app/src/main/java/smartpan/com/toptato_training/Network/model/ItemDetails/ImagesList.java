package smartpan.com.toptato_training.Network.model.ItemDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImagesList {

    @SerializedName("ImagePath")
    @Expose
    private String imagePath;
    @SerializedName("ColorId")
    @Expose
    private int colorId;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

}