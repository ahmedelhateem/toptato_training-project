package smartpan.com.toptato_training.Network.model.ContactUS;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactUsResponse {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("arabicMessage")
    @Expose
    private String arabicMessage;
    @SerializedName("englishMessage")
    @Expose
    private String englishMessage;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getArabicMessage() {
        return arabicMessage;
    }

    public void setArabicMessage(String arabicMessage) {
        this.arabicMessage = arabicMessage;
    }

    public String getEnglishMessage() {
        return englishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        this.englishMessage = englishMessage;
    }
}
