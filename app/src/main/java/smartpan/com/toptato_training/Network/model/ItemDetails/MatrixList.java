package smartpan.com.toptato_training.Network.model.ItemDetails;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatrixList {

    @SerializedName("SizeId")
    @Expose
    private int sizeId;
    @SerializedName("SizeArabicName")
    @Expose
    private String sizeArabicName;
    @SerializedName("SizeEnglishName")
    @Expose
    private String sizeEnglishName;
    @SerializedName("colors")
    @Expose
    private List<Color> colors = null;

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeArabicName() {
        return sizeArabicName;
    }

    public void setSizeArabicName(String sizeArabicName) {
        this.sizeArabicName = sizeArabicName;
    }

    public String getSizeEnglishName() {
        return sizeEnglishName;
    }

    public void setSizeEnglishName(String sizeEnglishName) {
        this.sizeEnglishName = sizeEnglishName;
    }

    public List<Color> getColors() {
        return colors;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }

}