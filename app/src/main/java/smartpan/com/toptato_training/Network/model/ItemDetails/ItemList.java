package smartpan.com.toptato_training.Network.model.ItemDetails;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemList {

    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("RefNumber")
    @Expose
    private Object refNumber;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("DescAR")
    @Expose
    private String descAR;
    @SerializedName("DescEN")
    @Expose
    private String descEN;
    @SerializedName("ArabicDetails")
    @Expose
    private String arabicDetails;
    @SerializedName("EnglishDetails")
    @Expose
    private String englishDetails;
    @SerializedName("CategoryImage")
    @Expose
    private String categoryImage;
    @SerializedName("CategoryVideo")
    @Expose
    private String categoryVideo;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("IsNewItem")
    @Expose
    private Object isNewItem;
    @SerializedName("UnitPrice")
    @Expose
    private int unitPrice;
    @SerializedName("ReviewStars")
    @Expose
    private int reviewStars;
    @SerializedName("IsPromotion")
    @Expose
    private boolean isPromotion;
    @SerializedName("PromotionUnitPrice")
    @Expose
    private int promotionUnitPrice;
    @SerializedName("RecieveDetails")
    @Expose
    private List<Object> recieveDetails = null;
    @SerializedName("IsActive")
    @Expose
    private boolean isActive;
    @SerializedName("CategoryId")
    @Expose
    private Object categoryId;
    @SerializedName("CategoryArabicName")
    @Expose
    private Object categoryArabicName;
    @SerializedName("CategoryEnglishName")
    @Expose
    private Object categoryEnglishName;
    @SerializedName("BrandId")
    @Expose
    private Object brandId;
    @SerializedName("BrandArabicName")
    @Expose
    private Object brandArabicName;
    @SerializedName("BrandEnglishName")
    @Expose
    private Object brandEnglishName;
    @SerializedName("ImagesList")
    @Expose
    private List<ImagesList> imagesList = null;
    @SerializedName("MatrixList")
    @Expose
    private List<MatrixList> matrixList = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(Object refNumber) {
        this.refNumber = refNumber;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getDescAR() {
        return descAR;
    }

    public void setDescAR(String descAR) {
        this.descAR = descAR;
    }

    public String getDescEN() {
        return descEN;
    }

    public void setDescEN(String descEN) {
        this.descEN = descEN;
    }

    public String getArabicDetails() {
        return arabicDetails;
    }

    public void setArabicDetails(String arabicDetails) {
        this.arabicDetails = arabicDetails;
    }

    public String getEnglishDetails() {
        return englishDetails;
    }

    public void setEnglishDetails(String englishDetails) {
        this.englishDetails = englishDetails;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public void setCategoryVideo(String categoryVideo) {
        this.categoryVideo = categoryVideo;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Object getIsNewItem() {
        return isNewItem;
    }

    public void setIsNewItem(Object isNewItem) {
        this.isNewItem = isNewItem;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getReviewStars() {
        return reviewStars;
    }

    public void setReviewStars(int reviewStars) {
        this.reviewStars = reviewStars;
    }

    public boolean isIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(boolean isPromotion) {
        this.isPromotion = isPromotion;
    }

    public int getPromotionUnitPrice() {
        return promotionUnitPrice;
    }

    public void setPromotionUnitPrice(int promotionUnitPrice) {
        this.promotionUnitPrice = promotionUnitPrice;
    }

    public List<Object> getRecieveDetails() {
        return recieveDetails;
    }

    public void setRecieveDetails(List<Object> recieveDetails) {
        this.recieveDetails = recieveDetails;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Object getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Object categoryId) {
        this.categoryId = categoryId;
    }

    public Object getCategoryArabicName() {
        return categoryArabicName;
    }

    public void setCategoryArabicName(Object categoryArabicName) {
        this.categoryArabicName = categoryArabicName;
    }

    public Object getCategoryEnglishName() {
        return categoryEnglishName;
    }

    public void setCategoryEnglishName(Object categoryEnglishName) {
        this.categoryEnglishName = categoryEnglishName;
    }

    public Object getBrandId() {
        return brandId;
    }

    public void setBrandId(Object brandId) {
        this.brandId = brandId;
    }

    public Object getBrandArabicName() {
        return brandArabicName;
    }

    public void setBrandArabicName(Object brandArabicName) {
        this.brandArabicName = brandArabicName;
    }

    public Object getBrandEnglishName() {
        return brandEnglishName;
    }

    public void setBrandEnglishName(Object brandEnglishName) {
        this.brandEnglishName = brandEnglishName;
    }

    public List<ImagesList> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<ImagesList> imagesList) {
        this.imagesList = imagesList;
    }

    public List<MatrixList> getMatrixList() {
        return matrixList;
    }

    public void setMatrixList(List<MatrixList> matrixList) {
        this.matrixList = matrixList;
    }

}