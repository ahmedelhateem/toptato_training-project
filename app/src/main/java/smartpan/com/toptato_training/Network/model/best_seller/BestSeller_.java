package smartpan.com.toptato_training.Network.model.best_seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import smartpan.com.toptato_training.Network.model.best_offers.ColorList;
import smartpan.com.toptato_training.Network.model.best_offers.SizeList;

public class BestSeller_ {
    @SerializedName("Id")
    @Expose
    private long id;
    @SerializedName("RefNumber")
    @Expose
    private Object refNumber;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("DescAR")
    @Expose
    private Object descAR;
    @SerializedName("DescEN")
    @Expose
    private Object descEN;
    @SerializedName("ArabicDetails")
    @Expose
    private Object arabicDetails;
    @SerializedName("EnglishDetails")
    @Expose
    private Object englishDetails;
    @SerializedName("CategoryImage")
    @Expose
    private String categoryImage;
    @SerializedName("CategoryVideo")
    @Expose
    private String categoryVideo;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("IsNewItem")
    @Expose
    private boolean isNewItem;
    @SerializedName("UnitPrice")
    @Expose
    private long unitPrice;
    @SerializedName("ReviewStars")
    @Expose
    private long reviewStars;
    @SerializedName("IsPromotion")
    @Expose
    private boolean isPromotion;
    @SerializedName("PromotionUnitPrice")
    @Expose
    private long promotionUnitPrice;
    @SerializedName("RecieveDetails")
    @Expose
    private List<Object> recieveDetails = new ArrayList<Object>();
    @SerializedName("IsActive")
    @Expose
    private boolean isActive;
    @SerializedName("ItemOrder")
    @Expose
    private long itemOrder;
    @SerializedName("CategoryId")
    @Expose
    private long categoryId;
    @SerializedName("CategoryArabicName")
    @Expose
    private String categoryArabicName;
    @SerializedName("CategoryEnglishName")
    @Expose
    private String categoryEnglishName;
    @SerializedName("BrandId")
    @Expose
    private long brandId;
    @SerializedName("BrandArabicName")
    @Expose
    private String brandArabicName;
    @SerializedName("BrandEnglishName")
    @Expose
    private String brandEnglishName;
    @SerializedName("ColorList")
    @Expose
    private List<ColorList> colorList = new ArrayList<ColorList>();
    @SerializedName("SizeList")
    @Expose
    private List<SizeList> sizeList = new ArrayList<SizeList>();

    /**
     * No args constructor for use in serialization
     *
     */
    public BestSeller_() {
    }

    /**
     *
     * @param englishName
     * @param isPromotion
     * @param categoryImage
     * @param recieveDetails
     * @param itemOrder
     * @param itemCode
     * @param sizeList
     * @param arabicDetails
     * @param categoryEnglishName
     * @param reviewStars
     * @param isActive
     * @param arabicName
     * @param descEN
     * @param brandArabicName
     * @param englishDetails
     * @param id
     * @param brandEnglishName
     * @param unitPrice
     * @param isNewItem
     * @param descAR
     * @param colorList
     * @param categoryVideo
     * @param refNumber
     * @param brandId
     * @param promotionUnitPrice
     * @param categoryArabicName
     * @param categoryId
     */
    public BestSeller_(long id, Object refNumber, String arabicName, String englishName, Object descAR, Object descEN, Object arabicDetails, Object englishDetails, String categoryImage, String categoryVideo, String itemCode, boolean isNewItem, long unitPrice, long reviewStars, boolean isPromotion, long promotionUnitPrice, List<Object> recieveDetails, boolean isActive, long itemOrder, long categoryId, String categoryArabicName, String categoryEnglishName, long brandId, String brandArabicName, String brandEnglishName, List<ColorList> colorList, List<SizeList> sizeList) {
        super();
        this.id = id;
        this.refNumber = refNumber;
        this.arabicName = arabicName;
        this.englishName = englishName;
        this.descAR = descAR;
        this.descEN = descEN;
        this.arabicDetails = arabicDetails;
        this.englishDetails = englishDetails;
        this.categoryImage = categoryImage;
        this.categoryVideo = categoryVideo;
        this.itemCode = itemCode;
        this.isNewItem = isNewItem;
        this.unitPrice = unitPrice;
        this.reviewStars = reviewStars;
        this.isPromotion = isPromotion;
        this.promotionUnitPrice = promotionUnitPrice;
        this.recieveDetails = recieveDetails;
        this.isActive = isActive;
        this.itemOrder = itemOrder;
        this.categoryId = categoryId;
        this.categoryArabicName = categoryArabicName;
        this.categoryEnglishName = categoryEnglishName;
        this.brandId = brandId;
        this.brandArabicName = brandArabicName;
        this.brandEnglishName = brandEnglishName;
        this.colorList = colorList;
        this.sizeList = sizeList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BestSeller_ withId(long id) {
        this.id = id;
        return this;
    }

    public Object getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(Object refNumber) {
        this.refNumber = refNumber;
    }

    public BestSeller_ withRefNumber(Object refNumber) {
        this.refNumber = refNumber;
        return this;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public BestSeller_ withArabicName(String arabicName) {
        this.arabicName = arabicName;
        return this;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public BestSeller_ withEnglishName(String englishName) {
        this.englishName = englishName;
        return this;
    }

    public Object getDescAR() {
        return descAR;
    }

    public void setDescAR(Object descAR) {
        this.descAR = descAR;
    }

    public BestSeller_ withDescAR(Object descAR) {
        this.descAR = descAR;
        return this;
    }

    public Object getDescEN() {
        return descEN;
    }

    public void setDescEN(Object descEN) {
        this.descEN = descEN;
    }

    public BestSeller_ withDescEN(Object descEN) {
        this.descEN = descEN;
        return this;
    }

    public Object getArabicDetails() {
        return arabicDetails;
    }

    public void setArabicDetails(Object arabicDetails) {
        this.arabicDetails = arabicDetails;
    }

    public BestSeller_ withArabicDetails(Object arabicDetails) {
        this.arabicDetails = arabicDetails;
        return this;
    }

    public Object getEnglishDetails() {
        return englishDetails;
    }

    public void setEnglishDetails(Object englishDetails) {
        this.englishDetails = englishDetails;
    }

    public BestSeller_ withEnglishDetails(Object englishDetails) {
        this.englishDetails = englishDetails;
        return this;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public BestSeller_ withCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
        return this;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public void setCategoryVideo(String categoryVideo) {
        this.categoryVideo = categoryVideo;
    }

    public BestSeller_ withCategoryVideo(String categoryVideo) {
        this.categoryVideo = categoryVideo;
        return this;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public BestSeller_ withItemCode(String itemCode) {
        this.itemCode = itemCode;
        return this;
    }

    public boolean isIsNewItem() {
        return isNewItem;
    }

    public void setIsNewItem(boolean isNewItem) {
        this.isNewItem = isNewItem;
    }

    public BestSeller_ withIsNewItem(boolean isNewItem) {
        this.isNewItem = isNewItem;
        return this;
    }

    public long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BestSeller_ withUnitPrice(long unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public long getReviewStars() {
        return reviewStars;
    }

    public void setReviewStars(long reviewStars) {
        this.reviewStars = reviewStars;
    }

    public BestSeller_ withReviewStars(long reviewStars) {
        this.reviewStars = reviewStars;
        return this;
    }

    public boolean isIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(boolean isPromotion) {
        this.isPromotion = isPromotion;
    }

    public BestSeller_ withIsPromotion(boolean isPromotion) {
        this.isPromotion = isPromotion;
        return this;
    }

    public long getPromotionUnitPrice() {
        return promotionUnitPrice;
    }

    public void setPromotionUnitPrice(long promotionUnitPrice) {
        this.promotionUnitPrice = promotionUnitPrice;
    }

    public BestSeller_ withPromotionUnitPrice(long promotionUnitPrice) {
        this.promotionUnitPrice = promotionUnitPrice;
        return this;
    }

    public List<Object> getRecieveDetails() {
        return recieveDetails;
    }

    public void setRecieveDetails(List<Object> recieveDetails) {
        this.recieveDetails = recieveDetails;
    }

    public BestSeller_ withRecieveDetails(List<Object> recieveDetails) {
        this.recieveDetails = recieveDetails;
        return this;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public BestSeller_ withIsActive(boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public long getItemOrder() {
        return itemOrder;
    }

    public void setItemOrder(long itemOrder) {
        this.itemOrder = itemOrder;
    }

    public BestSeller_ withItemOrder(long itemOrder) {
        this.itemOrder = itemOrder;
        return this;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public BestSeller_ withCategoryId(long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getCategoryArabicName() {
        return categoryArabicName;
    }

    public void setCategoryArabicName(String categoryArabicName) {
        this.categoryArabicName = categoryArabicName;
    }

    public BestSeller_ withCategoryArabicName(String categoryArabicName) {
        this.categoryArabicName = categoryArabicName;
        return this;
    }

    public String getCategoryEnglishName() {
        return categoryEnglishName;
    }

    public void setCategoryEnglishName(String categoryEnglishName) {
        this.categoryEnglishName = categoryEnglishName;
    }

    public BestSeller_ withCategoryEnglishName(String categoryEnglishName) {
        this.categoryEnglishName = categoryEnglishName;
        return this;
    }

    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    public BestSeller_ withBrandId(long brandId) {
        this.brandId = brandId;
        return this;
    }

    public String getBrandArabicName() {
        return brandArabicName;
    }

    public void setBrandArabicName(String brandArabicName) {
        this.brandArabicName = brandArabicName;
    }

    public BestSeller_ withBrandArabicName(String brandArabicName) {
        this.brandArabicName = brandArabicName;
        return this;
    }

    public String getBrandEnglishName() {
        return brandEnglishName;
    }

    public void setBrandEnglishName(String brandEnglishName) {
        this.brandEnglishName = brandEnglishName;
    }

    public BestSeller_ withBrandEnglishName(String brandEnglishName) {
        this.brandEnglishName = brandEnglishName;
        return this;
    }

    public List<ColorList> getColorList() {
        return colorList;
    }

    public void setColorList(List<ColorList> colorList) {
        this.colorList = colorList;
    }

    public BestSeller_ withColorList(List<ColorList> colorList) {
        this.colorList = colorList;
        return this;
    }

    public List<SizeList> getSizeList() {
        return sizeList;
    }

    public void setSizeList(List<SizeList> sizeList) {
        this.sizeList = sizeList;
    }

    public BestSeller_ withSizeList(List<SizeList> sizeList) {
        this.sizeList = sizeList;
        return this;
    }
}
