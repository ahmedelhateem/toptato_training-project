package smartpan.com.toptato_training.Network.model.slider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SiteSliderList {

    @SerializedName("Id")
    @Expose
    private long id;
    @SerializedName("ArabicTitle")
    @Expose
    private String arabicTitle;
    @SerializedName("EnglishTitle")
    @Expose
    private String englishTitle;
    @SerializedName("ContentAR")
    @Expose
    private Object contentAR;
    @SerializedName("ContentEN")
    @Expose
    private Object contentEN;
    @SerializedName("HeaderAR")
    @Expose
    private Object headerAR;
    @SerializedName("HeaderEN")
    @Expose
    private Object headerEN;
    @SerializedName("Sequence")
    @Expose
    private long sequence;
    @SerializedName("StoryImage")
    @Expose
    private String storyImage;
    @SerializedName("BackGroundImage")
    @Expose
    private String backGroundImage;

    /**
     * No args constructor for use in serialization
     *
     */
    public SiteSliderList() {
    }

    /**
     *
     * @param arabicTitle
     * @param headerEN
     * @param sequence
     * @param contentAR
     * @param headerAR
     * @param backGroundImage
     * @param id
     * @param storyImage
     * @param contentEN
     * @param englishTitle
     */
    public SiteSliderList(long id, String arabicTitle, String englishTitle, Object contentAR, Object contentEN, Object headerAR, Object headerEN, long sequence, String storyImage, String backGroundImage) {
        super();
        this.id = id;
        this.arabicTitle = arabicTitle;
        this.englishTitle = englishTitle;
        this.contentAR = contentAR;
        this.contentEN = contentEN;
        this.headerAR = headerAR;
        this.headerEN = headerEN;
        this.sequence = sequence;
        this.storyImage = storyImage;
        this.backGroundImage = backGroundImage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SiteSliderList withId(long id) {
        this.id = id;
        return this;
    }

    public String getArabicTitle() {
        return arabicTitle;
    }

    public void setArabicTitle(String arabicTitle) {
        this.arabicTitle = arabicTitle;
    }

    public SiteSliderList withArabicTitle(String arabicTitle) {
        this.arabicTitle = arabicTitle;
        return this;
    }

    public String getEnglishTitle() {
        return englishTitle;
    }

    public void setEnglishTitle(String englishTitle) {
        this.englishTitle = englishTitle;
    }

    public SiteSliderList withEnglishTitle(String englishTitle) {
        this.englishTitle = englishTitle;
        return this;
    }

    public Object getContentAR() {
        return contentAR;
    }

    public void setContentAR(Object contentAR) {
        this.contentAR = contentAR;
    }

    public SiteSliderList withContentAR(Object contentAR) {
        this.contentAR = contentAR;
        return this;
    }

    public Object getContentEN() {
        return contentEN;
    }

    public void setContentEN(Object contentEN) {
        this.contentEN = contentEN;
    }

    public SiteSliderList withContentEN(Object contentEN) {
        this.contentEN = contentEN;
        return this;
    }

    public Object getHeaderAR() {
        return headerAR;
    }

    public void setHeaderAR(Object headerAR) {
        this.headerAR = headerAR;
    }

    public SiteSliderList withHeaderAR(Object headerAR) {
        this.headerAR = headerAR;
        return this;
    }

    public Object getHeaderEN() {
        return headerEN;
    }

    public void setHeaderEN(Object headerEN) {
        this.headerEN = headerEN;
    }

    public SiteSliderList withHeaderEN(Object headerEN) {
        this.headerEN = headerEN;
        return this;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    public SiteSliderList withSequence(long sequence) {
        this.sequence = sequence;
        return this;
    }

    public String getStoryImage() {
        return storyImage;
    }

    public void setStoryImage(String storyImage) {
        this.storyImage = storyImage;
    }

    public SiteSliderList withStoryImage(String storyImage) {
        this.storyImage = storyImage;
        return this;
    }

    public String getBackGroundImage() {
        return backGroundImage;
    }

    public void setBackGroundImage(String backGroundImage) {
        this.backGroundImage = backGroundImage;
    }

    public SiteSliderList withBackGroundImage(String backGroundImage) {
        this.backGroundImage = backGroundImage;
        return this;
    }

}