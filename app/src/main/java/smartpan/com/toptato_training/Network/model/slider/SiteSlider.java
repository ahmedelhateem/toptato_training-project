package smartpan.com.toptato_training.Network.model.slider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SiteSlider {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("SiteSliderList")
    @Expose
    private List<SiteSliderList> siteSliderList = new ArrayList<SiteSliderList>();

    /**
     * No args constructor for use in serialization
     *
     */
    public SiteSlider() {
    }

    /**
     *
     * @param siteSliderList
     * @param success
     */
    public SiteSlider(String success, List<SiteSliderList> siteSliderList) {
        super();
        this.success = success;
        this.siteSliderList = siteSliderList;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public SiteSlider withSuccess(String success) {
        this.success = success;
        return this;
    }

    public List<SiteSliderList> getSiteSliderList() {
        return siteSliderList;
    }

    public void setSiteSliderList(List<SiteSliderList> siteSliderList) {
        this.siteSliderList = siteSliderList;
    }

    public SiteSlider withSiteSliderList(List<SiteSliderList> siteSliderList) {
        this.siteSliderList = siteSliderList;
        return this;
    }

}