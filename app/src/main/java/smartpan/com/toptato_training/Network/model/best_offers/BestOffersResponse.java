package smartpan.com.toptato_training.Network.model.best_offers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BestOffersResponse {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("BestOffers")
    @Expose
    private List<BestOffer> bestOffers = new ArrayList<BestOffer>();

    /**
     * No args constructor for use in serialization
     *
     */
    public BestOffersResponse() {
    }

    /**
     *
     * @param success
     * @param bestOffers
     */
    public BestOffersResponse(String success, List<BestOffer> bestOffers) {
        super();
        this.success = success;
        this.bestOffers = bestOffers;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public BestOffersResponse withSuccess(String success) {
        this.success = success;
        return this;
    }

    public List<BestOffer> getBestOffers() {
        return bestOffers;
    }

    public void setBestOffers(List<BestOffer> bestOffers) {
        this.bestOffers = bestOffers;
    }

    public BestOffersResponse withBestOffers(List<BestOffer> bestOffers) {
        this.bestOffers = bestOffers;
        return this;
    }

}