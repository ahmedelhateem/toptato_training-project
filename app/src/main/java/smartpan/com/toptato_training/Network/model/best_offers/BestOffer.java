package smartpan.com.toptato_training.Network.model.best_offers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BestOffer {

    @SerializedName("Id")
    @Expose
    private long id;
    @SerializedName("RefNumber")
    @Expose
    private String refNumber;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("DescAR")
    @Expose
    private Object descAR;
    @SerializedName("DescEN")
    @Expose
    private Object descEN;
    @SerializedName("ArabicDetails")
    @Expose
    private String arabicDetails;
    @SerializedName("EnglishDetails")
    @Expose
    private Object englishDetails;
    @SerializedName("CategoryImage")
    @Expose
    private String categoryImage;
    @SerializedName("CategoryVideo")
    @Expose
    private String categoryVideo;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("IsNewItem")
    @Expose
    private boolean isNewItem;
    @SerializedName("UnitPrice")
    @Expose
    private long unitPrice;
    @SerializedName("ReviewStars")
    @Expose
    private long reviewStars;
    @SerializedName("IsPromotion")
    @Expose
    private boolean isPromotion;
    @SerializedName("ItemOrder")
    @Expose
    private long itemOrder;
    @SerializedName("PromotionUnitPrice")
    @Expose
    private long promotionUnitPrice;
    @SerializedName("RecieveDetails")
    @Expose
    private List<Object> recieveDetails = new ArrayList<Object>();
    @SerializedName("IsActive")
    @Expose
    private boolean isActive;
    @SerializedName("CategoryId")
    @Expose
    private long categoryId;
    @SerializedName("CategoryArabicName")
    @Expose
    private String categoryArabicName;
    @SerializedName("CategoryEnglishName")
    @Expose
    private String categoryEnglishName;
    @SerializedName("BrandId")
    @Expose
    private long brandId;
    @SerializedName("BrandArabicName")
    @Expose
    private String brandArabicName;
    @SerializedName("BrandEnglishName")
    @Expose
    private String brandEnglishName;
    @SerializedName("ColorList")
    @Expose
    private List<ColorList> colorList = new ArrayList<ColorList>();
    @SerializedName("SizeList")
    @Expose
    private List<SizeList> sizeList = new ArrayList<SizeList>();

    /**
     * No args constructor for use in serialization
     *
     */
    public BestOffer() {
    }

    /**
     *
     * @param englishName
     * @param isPromotion
     * @param categoryImage
     * @param itemOrder
     * @param recieveDetails
     * @param itemCode
     * @param sizeList
     * @param arabicDetails
     * @param categoryEnglishName
     * @param reviewStars
     * @param isActive
     * @param arabicName
     * @param descEN
     * @param brandArabicName
     * @param englishDetails
     * @param id
     * @param brandEnglishName
     * @param unitPrice
     * @param isNewItem
     * @param descAR
     * @param colorList
     * @param categoryVideo
     * @param refNumber
     * @param brandId
     * @param promotionUnitPrice
     * @param categoryArabicName
     * @param categoryId
     */
    public BestOffer(long id, String refNumber, String arabicName, String englishName, Object descAR, Object descEN, String arabicDetails, Object englishDetails, String categoryImage, String categoryVideo, String itemCode, boolean isNewItem, long unitPrice, long reviewStars, boolean isPromotion, long itemOrder, long promotionUnitPrice, List<Object> recieveDetails, boolean isActive, long categoryId, String categoryArabicName, String categoryEnglishName, long brandId, String brandArabicName, String brandEnglishName, List<ColorList> colorList, List<SizeList> sizeList) {
        super();
        this.id = id;
        this.refNumber = refNumber;
        this.arabicName = arabicName;
        this.englishName = englishName;
        this.descAR = descAR;
        this.descEN = descEN;
        this.arabicDetails = arabicDetails;
        this.englishDetails = englishDetails;
        this.categoryImage = categoryImage;
        this.categoryVideo = categoryVideo;
        this.itemCode = itemCode;
        this.isNewItem = isNewItem;
        this.unitPrice = unitPrice;
        this.reviewStars = reviewStars;
        this.isPromotion = isPromotion;
        this.itemOrder = itemOrder;
        this.promotionUnitPrice = promotionUnitPrice;
        this.recieveDetails = recieveDetails;
        this.isActive = isActive;
        this.categoryId = categoryId;
        this.categoryArabicName = categoryArabicName;
        this.categoryEnglishName = categoryEnglishName;
        this.brandId = brandId;
        this.brandArabicName = brandArabicName;
        this.brandEnglishName = brandEnglishName;
        this.colorList = colorList;
        this.sizeList = sizeList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BestOffer withId(long id) {
        this.id = id;
        return this;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public BestOffer withRefNumber(String refNumber) {
        this.refNumber = refNumber;
        return this;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public BestOffer withArabicName(String arabicName) {
        this.arabicName = arabicName;
        return this;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public BestOffer withEnglishName(String englishName) {
        this.englishName = englishName;
        return this;
    }

    public Object getDescAR() {
        return descAR;
    }

    public void setDescAR(Object descAR) {
        this.descAR = descAR;
    }

    public BestOffer withDescAR(Object descAR) {
        this.descAR = descAR;
        return this;
    }

    public Object getDescEN() {
        return descEN;
    }

    public void setDescEN(Object descEN) {
        this.descEN = descEN;
    }

    public BestOffer withDescEN(Object descEN) {
        this.descEN = descEN;
        return this;
    }

    public String getArabicDetails() {
        return arabicDetails;
    }

    public void setArabicDetails(String arabicDetails) {
        this.arabicDetails = arabicDetails;
    }

    public BestOffer withArabicDetails(String arabicDetails) {
        this.arabicDetails = arabicDetails;
        return this;
    }

    public Object getEnglishDetails() {
        return englishDetails;
    }

    public void setEnglishDetails(Object englishDetails) {
        this.englishDetails = englishDetails;
    }

    public BestOffer withEnglishDetails(Object englishDetails) {
        this.englishDetails = englishDetails;
        return this;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public BestOffer withCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
        return this;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public void setCategoryVideo(String categoryVideo) {
        this.categoryVideo = categoryVideo;
    }

    public BestOffer withCategoryVideo(String categoryVideo) {
        this.categoryVideo = categoryVideo;
        return this;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public BestOffer withItemCode(String itemCode) {
        this.itemCode = itemCode;
        return this;
    }

    public boolean isIsNewItem() {
        return isNewItem;
    }

    public void setIsNewItem(boolean isNewItem) {
        this.isNewItem = isNewItem;
    }

    public BestOffer withIsNewItem(boolean isNewItem) {
        this.isNewItem = isNewItem;
        return this;
    }

    public long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BestOffer withUnitPrice(long unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public long getReviewStars() {
        return reviewStars;
    }

    public void setReviewStars(long reviewStars) {
        this.reviewStars = reviewStars;
    }

    public BestOffer withReviewStars(long reviewStars) {
        this.reviewStars = reviewStars;
        return this;
    }

    public boolean isIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(boolean isPromotion) {
        this.isPromotion = isPromotion;
    }

    public BestOffer withIsPromotion(boolean isPromotion) {
        this.isPromotion = isPromotion;
        return this;
    }

    public long getItemOrder() {
        return itemOrder;
    }

    public void setItemOrder(long itemOrder) {
        this.itemOrder = itemOrder;
    }

    public BestOffer withItemOrder(long itemOrder) {
        this.itemOrder = itemOrder;
        return this;
    }

    public long getPromotionUnitPrice() {
        return promotionUnitPrice;
    }

    public void setPromotionUnitPrice(long promotionUnitPrice) {
        this.promotionUnitPrice = promotionUnitPrice;
    }

    public BestOffer withPromotionUnitPrice(long promotionUnitPrice) {
        this.promotionUnitPrice = promotionUnitPrice;
        return this;
    }

    public List<Object> getRecieveDetails() {
        return recieveDetails;
    }

    public void setRecieveDetails(List<Object> recieveDetails) {
        this.recieveDetails = recieveDetails;
    }

    public BestOffer withRecieveDetails(List<Object> recieveDetails) {
        this.recieveDetails = recieveDetails;
        return this;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public BestOffer withIsActive(boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public BestOffer withCategoryId(long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getCategoryArabicName() {
        return categoryArabicName;
    }

    public void setCategoryArabicName(String categoryArabicName) {
        this.categoryArabicName = categoryArabicName;
    }

    public BestOffer withCategoryArabicName(String categoryArabicName) {
        this.categoryArabicName = categoryArabicName;
        return this;
    }

    public String getCategoryEnglishName() {
        return categoryEnglishName;
    }

    public void setCategoryEnglishName(String categoryEnglishName) {
        this.categoryEnglishName = categoryEnglishName;
    }

    public BestOffer withCategoryEnglishName(String categoryEnglishName) {
        this.categoryEnglishName = categoryEnglishName;
        return this;
    }

    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    public BestOffer withBrandId(long brandId) {
        this.brandId = brandId;
        return this;
    }

    public String getBrandArabicName() {
        return brandArabicName;
    }

    public void setBrandArabicName(String brandArabicName) {
        this.brandArabicName = brandArabicName;
    }

    public BestOffer withBrandArabicName(String brandArabicName) {
        this.brandArabicName = brandArabicName;
        return this;
    }

    public String getBrandEnglishName() {
        return brandEnglishName;
    }

    public void setBrandEnglishName(String brandEnglishName) {
        this.brandEnglishName = brandEnglishName;
    }

    public BestOffer withBrandEnglishName(String brandEnglishName) {
        this.brandEnglishName = brandEnglishName;
        return this;
    }

    public List<ColorList> getColorList() {
        return colorList;
    }

    public void setColorList(List<ColorList> colorList) {
        this.colorList = colorList;
    }

    public BestOffer withColorList(List<ColorList> colorList) {
        this.colorList = colorList;
        return this;
    }

    public List<SizeList> getSizeList() {
        return sizeList;
    }

    public void setSizeList(List<SizeList> sizeList) {
        this.sizeList = sizeList;
    }

    public BestOffer withSizeList(List<SizeList> sizeList) {
        this.sizeList = sizeList;
        return this;
    }

}