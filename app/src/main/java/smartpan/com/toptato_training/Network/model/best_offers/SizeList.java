package smartpan.com.toptato_training.Network.model.best_offers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SizeList {

    @SerializedName("SizeId")
    @Expose
    private long sizeId;
    @SerializedName("ItemSizeArabicName")
    @Expose
    private String itemSizeArabicName;
    @SerializedName("ItemSizeEnglishName")
    @Expose
    private String itemSizeEnglishName;

    /**
     * No args constructor for use in serialization
     *
     */
    public SizeList() {
    }

    /**
     *
     * @param sizeId
     * @param itemSizeArabicName
     * @param itemSizeEnglishName
     */
    public SizeList(long sizeId, String itemSizeArabicName, String itemSizeEnglishName) {
        super();
        this.sizeId = sizeId;
        this.itemSizeArabicName = itemSizeArabicName;
        this.itemSizeEnglishName = itemSizeEnglishName;
    }

    public long getSizeId() {
        return sizeId;
    }

    public void setSizeId(long sizeId) {
        this.sizeId = sizeId;
    }

    public SizeList withSizeId(long sizeId) {
        this.sizeId = sizeId;
        return this;
    }

    public String getItemSizeArabicName() {
        return itemSizeArabicName;
    }

    public void setItemSizeArabicName(String itemSizeArabicName) {
        this.itemSizeArabicName = itemSizeArabicName;
    }

    public SizeList withItemSizeArabicName(String itemSizeArabicName) {
        this.itemSizeArabicName = itemSizeArabicName;
        return this;
    }

    public String getItemSizeEnglishName() {
        return itemSizeEnglishName;
    }

    public void setItemSizeEnglishName(String itemSizeEnglishName) {
        this.itemSizeEnglishName = itemSizeEnglishName;
    }

    public SizeList withItemSizeEnglishName(String itemSizeEnglishName) {
        this.itemSizeEnglishName = itemSizeEnglishName;
        return this;
    }

}