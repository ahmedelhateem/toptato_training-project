package smartpan.com.toptato_training.Network.model.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;


public class CategoriesResponse {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("CategoryList")
    @Expose
    private List<CategoryList> categoryList = null;
    @SerializedName("message")
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<CategoryList> getCategoryList() {
        if (categoryList == null) {
            categoryList = Collections.emptyList();
        }

        return categoryList;
    }

    public void setCategoryList(List<CategoryList> categoryList) {
        this.categoryList = categoryList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
