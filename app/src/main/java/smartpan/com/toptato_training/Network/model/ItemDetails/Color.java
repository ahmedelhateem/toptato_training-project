package smartpan.com.toptato_training.Network.model.ItemDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Color {

    @SerializedName("ColorId")
    @Expose
    private int colorId;
    @SerializedName("ColorArabicName")
    @Expose
    private String colorArabicName;
    @SerializedName("ColorEnglishName")
    @Expose
    private String colorEnglishName;
    @SerializedName("ColorCode")
    @Expose
    private String colorCode;
    @SerializedName("AvailableQuantity")
    @Expose
    private int availableQuantity;

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public String getColorArabicName() {
        return colorArabicName;
    }

    public void setColorArabicName(String colorArabicName) {
        this.colorArabicName = colorArabicName;
    }

    public String getColorEnglishName() {
        return colorEnglishName;
    }

    public void setColorEnglishName(String colorEnglishName) {
        this.colorEnglishName = colorEnglishName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

}