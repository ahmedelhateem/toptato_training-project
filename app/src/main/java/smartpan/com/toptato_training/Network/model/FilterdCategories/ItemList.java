package smartpan.com.toptato_training.Network.model.FilterdCategories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemList {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("RefNumber")
    @Expose
    private Object refNumber;
    @SerializedName("ArabicName")
    @Expose
    private String arabicName;
    @SerializedName("EnglishName")
    @Expose
    private String englishName;
    @SerializedName("DescAR")
    @Expose
    private String descAR;
    @SerializedName("DescEN")
    @Expose
    private Object descEN;
    @SerializedName("ItemOrder")
    @Expose
    private Object itemOrder;
    @SerializedName("ArabicDetails")
    @Expose
    private String arabicDetails;
    @SerializedName("EnglishDetails")
    @Expose
    private Object englishDetails;
    @SerializedName("CategoryImage")
    @Expose
    private String categoryImage;
    @SerializedName("CategoryVideo")
    @Expose
    private String categoryVideo;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("IsNewItem")
    @Expose
    private Boolean isNewItem;
    @SerializedName("UnitPrice")
    @Expose
    private float unitPrice;
    @SerializedName("ReviewStars")
    @Expose
    private Integer reviewStars;
    @SerializedName("IsPromotion")
    @Expose
    private Boolean isPromotion;
    @SerializedName("PromotionUnitPrice")
    @Expose
    private float promotionUnitPrice;
    @SerializedName("RecieveDetails")
    @Expose
    private List<Object> recieveDetails = null;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("CategoryArabicName")
    @Expose
    private String categoryArabicName;
    @SerializedName("CategoryEnglishName")
    @Expose
    private String categoryEnglishName;
    @SerializedName("BrandId")
    @Expose
    private Integer brandId;
    @SerializedName("BrandArabicName")
    @Expose
    private String brandArabicName;
    @SerializedName("BrandEnglishName")
    @Expose
    private String brandEnglishName;
    @SerializedName("ColorList")
    @Expose
    private List<ColorList> colorList = null;
    @SerializedName("SizeList")
    @Expose
    private List<SizeList> sizeList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(Object refNumber) {
        this.refNumber = refNumber;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getDescAR() {
        return descAR;
    }

    public void setDescAR(String descAR) {
        this.descAR = descAR;
    }

    public Object getDescEN() {
        return descEN;
    }

    public void setDescEN(Object descEN) {
        this.descEN = descEN;
    }

    public Object getItemOrder() {
        return itemOrder;
    }

    public void setItemOrder(Object itemOrder) {
        this.itemOrder = itemOrder;
    }

    public String getArabicDetails() {
        return arabicDetails;
    }

    public void setArabicDetails(String arabicDetails) {
        this.arabicDetails = arabicDetails;
    }

    public Object getEnglishDetails() {
        return englishDetails;
    }

    public void setEnglishDetails(Object englishDetails) {
        this.englishDetails = englishDetails;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryVideo() {
        return categoryVideo;
    }

    public void setCategoryVideo(String categoryVideo) {
        this.categoryVideo = categoryVideo;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Boolean getIsNewItem() {
        return isNewItem;
    }

    public void setIsNewItem(Boolean isNewItem) {
        this.isNewItem = isNewItem;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getReviewStars() {
        return reviewStars;
    }

    public void setReviewStars(Integer reviewStars) {
        this.reviewStars = reviewStars;
    }

    public Boolean getIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(Boolean isPromotion) {
        this.isPromotion = isPromotion;
    }

    public float getPromotionUnitPrice() {
        return promotionUnitPrice;
    }

    public void setPromotionUnitPrice(Integer promotionUnitPrice) {
        this.promotionUnitPrice = promotionUnitPrice;
    }

    public List<Object> getRecieveDetails() {
        return recieveDetails;
    }

    public void setRecieveDetails(List<Object> recieveDetails) {
        this.recieveDetails = recieveDetails;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryArabicName() {
        return categoryArabicName;
    }

    public void setCategoryArabicName(String categoryArabicName) {
        this.categoryArabicName = categoryArabicName;
    }

    public String getCategoryEnglishName() {
        return categoryEnglishName;
    }

    public void setCategoryEnglishName(String categoryEnglishName) {
        this.categoryEnglishName = categoryEnglishName;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandArabicName() {
        return brandArabicName;
    }

    public void setBrandArabicName(String brandArabicName) {
        this.brandArabicName = brandArabicName;
    }

    public String getBrandEnglishName() {
        return brandEnglishName;
    }

    public void setBrandEnglishName(String brandEnglishName) {
        this.brandEnglishName = brandEnglishName;
    }

    public List<ColorList> getColorList() {
        return colorList;
    }

    public void setColorList(List<ColorList> colorList) {
        this.colorList = colorList;
    }

    public List<SizeList> getSizeList() {
        return sizeList;
    }

    public void setSizeList(List<SizeList> sizeList) {
        this.sizeList = sizeList;
    }

    @Override
    public String toString() {
        return "ItemList{" +
                "id=" + id +
                ", refNumber=" + refNumber +
                ", arabicName='" + arabicName + '\'' +
                ", englishName='" + englishName + '\'' +
                ", descAR='" + descAR + '\'' +
                ", descEN=" + descEN +
                ", itemOrder=" + itemOrder +
                ", arabicDetails='" + arabicDetails + '\'' +
                ", englishDetails=" + englishDetails +
                ", categoryImage='" + categoryImage + '\'' +
                ", categoryVideo='" + categoryVideo + '\'' +
                ", itemCode='" + itemCode + '\'' +
                ", isNewItem=" + isNewItem +
                ", unitPrice=" + unitPrice +
                ", reviewStars=" + reviewStars +
                ", isPromotion=" + isPromotion +
                ", promotionUnitPrice=" + promotionUnitPrice +
                ", recieveDetails=" + recieveDetails +
                ", isActive=" + isActive +
                ", categoryId=" + categoryId +
                ", categoryArabicName='" + categoryArabicName + '\'' +
                ", categoryEnglishName='" + categoryEnglishName + '\'' +
                ", brandId=" + brandId +
                ", brandArabicName='" + brandArabicName + '\'' +
                ", brandEnglishName='" + brandEnglishName + '\'' +
                ", colorList=" + colorList +
                ", sizeList=" + sizeList +
                '}';
    }
}