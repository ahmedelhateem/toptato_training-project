package smartpan.com.toptato_training.Retrofit;


import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import smartpan.com.toptato_training.Network.model.FilterdCategories.CategoryFilterResponse;
import smartpan.com.toptato_training.Network.model.best_offers.BestOffersResponse;
import smartpan.com.toptato_training.Network.model.best_seller.BestSeller;
import retrofit2.http.Query;
import smartpan.com.toptato_training.Network.model.ContactUS.ContactUsResponse;
import retrofit2.http.QueryMap;
import smartpan.com.toptato_training.Network.model.ItemDetails.ItemResponse;
import smartpan.com.toptato_training.Network.model.Registration.RegistrationResponse;
import smartpan.com.toptato_training.Network.model.category.CategoriesResponse;
import smartpan.com.toptato_training.Network.model.slash.TokenResponse;
import smartpan.com.toptato_training.Network.model.slider.SiteSlider;

import static smartpan.com.toptato_training.utils.Constants.HEADER_AUTHORIZATION;
import static smartpan.com.toptato_training.utils.Constants.Method_BestOffers;
import static smartpan.com.toptato_training.utils.Constants.Method_BestSeller;
import static smartpan.com.toptato_training.utils.Constants.Method_GetCategories;
import static smartpan.com.toptato_training.utils.Constants.Method_GetFilterItems;
import static smartpan.com.toptato_training.utils.Constants.Method_GetOffers;
import static smartpan.com.toptato_training.utils.Constants.Method_GetItemDetails;
import static smartpan.com.toptato_training.utils.Constants.Method_GetToken;
import static smartpan.com.toptato_training.utils.Constants.Method_SendContactMessage;
import static smartpan.com.toptato_training.utils.Constants.Method_SiteSlider;
import static smartpan.com.toptato_training.utils.Constants.Method_Register;

public interface ApiInterface {

    @GET(Method_GetCategories)
    Call<CategoriesResponse> getCategories(@Header(HEADER_AUTHORIZATION) String auth);

    @POST(Method_GetFilterItems)
    Call<CategoryFilterResponse> getFilteredCategories(@Header(HEADER_AUTHORIZATION) String auth, @QueryMap Map<String,String> parameters,@Body CategoryFilterResponse categoryFilterResponse);

    @POST(Method_GetToken)
    @FormUrlEncoded
    Call<TokenResponse> getToken(@Field("username") String userName, @Field("password") String password, @Field("grant_type") String grantType, @Field("deviceName") String deviceName);

    @POST(Method_GetItemDetails)
    @FormUrlEncoded
    Call<ItemResponse> getitem(@Header(HEADER_AUTHORIZATION) String auth, @Field("ItemId") int id);

    @POST(Method_Register)
    Call<RegistrationResponse> register(@Header(HEADER_AUTHORIZATION) String auth, @QueryMap Map<String,String> queryMap);

    @GET(Method_SiteSlider)
    Call<SiteSlider> getSliderImages(@Header(HEADER_AUTHORIZATION) String auth);

    @GET(Method_BestOffers)
    Call<BestOffersResponse> getBestOffers(@Header(HEADER_AUTHORIZATION) String auth);

    @GET(Method_BestSeller)
    Call<BestSeller> getBestSeller(@Header(HEADER_AUTHORIZATION) String auth);

    @POST(Method_SendContactMessage)
    Call<ContactUsResponse> contactUs( @Header(HEADER_AUTHORIZATION) String auth ,@Query("Name")  String name,@Query("Email")  String email, @Query("Message") String message);

    @POST(Method_GetOffers)
    Call<CategoryFilterResponse> getOffers(@Header(HEADER_AUTHORIZATION) String auth,@Body CategoryFilterResponse categoryFilterResponse);

}

