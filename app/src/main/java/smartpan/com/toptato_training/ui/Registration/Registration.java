package smartpan.com.toptato_training.ui.Registration;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.utils.Validation;


public class Registration extends AppCompatActivity implements RegistrationListener {
    Button next;
    RadioGroup radioGroup;
    RadioButton gender;
    EditText first,last,email,password,date;
    Toolbar toolbar;
    private Calendar mcalendar=Calendar.getInstance();
    private int day,month,year;
    boolean check;
    RegistrationPresenter registrationPresenter;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        toolbar=  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        next=findViewById(R.id.next);
        first=findViewById(R.id.first_name);
        last=findViewById(R.id.last_name);
        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        date=findViewById(R.id.date);
        radioGroup=findViewById(R.id.gender);
        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);
        registrationPresenter=new RegistrationPresenter(this,this);

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialog();
            }
        });

        Validation validation=new Validation();
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                gender = (RadioButton) findViewById(selectedId);
                check = validation.validate(first.getText().toString(),last.getText().toString(),email.getText().toString(),date.getText().toString(),password.getText().toString(),gender.getText().toString(),Registration.this);
                if(check){
                    if(gender.getText().toString().equals("male")){
                        gender.setText("true");
                    }
                    else {
                        gender.setText("false");
                    }
                    Map<String,String> map=new HashMap<>();
                    map.put("FirstName",first.getText().toString());
                    map.put("SecondName",last.getText().toString());
                    map.put("ImagePath","");
                    map.put("Mobile","011156633");
                    map.put("E_Mail",email.getText().toString());
                    map.put("LogInPassword",password.getText().toString());
                    map.put("BirthDay",date.getText().toString());
                    map.put("Gender",gender.getText().toString());
                    map.put("RecieveNewsLetter","true");
                    map.put("CountryId","1");
                    map.put("CityId","1");
                    map.put("DistricId","1");
                    map.put("StreetName","1");
                    map.put("Address","Kawmia");
                    registrationPresenter.getResponse(map);

                }



            }
        });

    }
    public void dateDialog(){
        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                date.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
            }};
        DatePickerDialog dpDialog=new DatePickerDialog(Registration.this, listener, year, month, day);
        dpDialog.show();
    }


    @Override
    public void check(String status) {
        Toast.makeText(this, status, Toast.LENGTH_SHORT).show();
    }
}
