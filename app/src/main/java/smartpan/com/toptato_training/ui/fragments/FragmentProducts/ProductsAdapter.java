package smartpan.com.toptato_training.ui.fragments.FragmentProducts;


import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import smartpan.com.toptato_training.Network.model.category.CategoryList;
import smartpan.com.toptato_training.R;


public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    Context context;
    List<CategoryList> categoriesList = new ArrayList<>();
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences.Editor editor;


    public ProductsAdapter(Context context) {
        this.context = context;
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

    }

    void setProductsList(List<CategoryList> categoriesList){
        if (categoriesList == null)
            return;
        this.categoriesList = categoriesList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row,parent,false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        CategoryList categoryList = categoriesList.get(position);
        Picasso.get().load(categoryList.getCategoryImage()).placeholder(R.drawable.no_pro).into(holder.productImage);
        holder.productTitle.setText(categoryList.getEnglishName());
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView productImage;
        TextView productTitle;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.product_image);
            productTitle = itemView.findViewById(R.id.product_text);
            //itemView.setOnClickListener(this);
        }



        @Override
        public void onClick(View view) {
        }
    }




}
