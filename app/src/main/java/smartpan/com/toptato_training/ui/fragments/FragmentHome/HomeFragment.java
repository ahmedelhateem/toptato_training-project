package smartpan.com.toptato_training.ui.fragments.FragmentHome;


import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.Objects;

import smartpan.com.toptato_training.Network.model.best_offers.BestOffer;
import smartpan.com.toptato_training.Network.model.best_seller.BestSeller_;
import smartpan.com.toptato_training.Network.model.category.CategoryList;
import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.fragments.FragmentRoot.HomeRootFragment;
import smartpan.com.toptato_training.ui.main.PagerAdapter;
import smartpan.com.toptato_training.utils.CenterZoomLayoutManager;
import smartpan.com.toptato_training.utils.Connectivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements HomeInterface {

    private HomePresenter homePresenter;
    private SliderAdapter adapter;
    private SliderView sliderView;
    private RecyclerView bestOfferRecyclerView;
    private BestOfferAdapter bestOfferAdapter;
    private RecyclerView categoryRecyclerView;
    private CategoryAdapter categoryAdapter;
    private RecyclerView bestSellerRecyclerView;
    private BestSellerAdapter bestSellerAdapter;



    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView;
        if (Connectivity.isNetworkAvailable(getActivity())) {     //if true
            rootView = inflater.inflate(R.layout.fragment_home, container, false);
        }
        else {      //if false
            rootView = inflater.inflate(R.layout.no_internet, container, false);
        }

        return rootView;
        //return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!Connectivity.isNetworkAvailable(getActivity())){
            Button button = view.findViewById(R.id.btn_check);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isNetworkAvailable(getActivity())){
                        getFragmentManager().beginTransaction()
                                .replace(R.id.home_root_frame, new HomeFragment()).commit();

                    }
                }
            });
        }
        homePresenter = new HomePresenter(getContext(), this);
        homePresenter.getSliderImages();
        homePresenter.getBestOffers();
        homePresenter.getCategories();
        homePresenter.getBestSeller();

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        sliderView = view.findViewById(R.id.imageSlider);
        bestOfferRecyclerView = view.findViewById(R.id.rv_best_offers);
        adapter = new SliderAdapter(getContext());

        categoryRecyclerView = view.findViewById(R.id.rv_categories);

        bestSellerRecyclerView = view.findViewById(R.id.rv_best_seller);
    }

    @Override
    public void setImageSlider(ArrayList<String> urls) {
        if (urls.isEmpty()) {
            Log.d("Empty request", "for slider");
        } else {
            for (String current : urls) {
                Log.d("Full request ", current);
            }
            adapter.setUrls(urls);
            setSlider();
        }
    }

    @Override
    public void setBestOffers(ArrayList<BestOffer> bestOffers) {
        if (bestOffers.isEmpty()){
            Log.d("Empty request", "for offers");
        }
        else {
            for (BestOffer bestOffer: bestOffers){
                Log.d("Description ", bestOffer.getEnglishName());
            }
            setBestOfferRecyclerView(bestOffers);
        }
    }

    @Override
    public void setCategories(ArrayList<CategoryList> categories) {
        if (categories.isEmpty()){
            Log.d("Empty request", "for categories");
        }
        else {
            for (CategoryList categoryList: categories){
                Log.d("Category: ", categoryList.getArabicName());
            }
            setCategoryRecyclerView(categories);
        }
    }

    @Override
    public void setBestSeller(ArrayList<BestSeller_> bestSeller) {
        if (bestSeller.isEmpty()){
            Log.d("Empty request", "for best seller");
        }else {
            for (BestSeller_ bestSeller_: bestSeller){
                Log.d("Best Seller Name", bestSeller_.getArabicName());
            }
            setBestSellerRecyclerView(bestSeller);
        }
    }

    private void setSlider() {
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(getContext().getResources().getColor(R.color.colorPrimaryDark));
        sliderView.setIndicatorUnselectedColor(Color.BLACK);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();
    }

    private void setBestOfferRecyclerView(ArrayList<BestOffer> bestOffers){
        bestOfferRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        bestOfferRecyclerView.setHasFixedSize(false);
        bestOfferAdapter = new BestOfferAdapter();
        bestOfferAdapter.setBestOffers(bestOffers);
        bestOfferRecyclerView.setAdapter(bestOfferAdapter);
    }

    private void setCategoryRecyclerView(ArrayList<CategoryList> categoryLists){
        categoryRecyclerView.setLayoutManager(new CenterZoomLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        categoryRecyclerView.setHasFixedSize(false);
        categoryAdapter = new CategoryAdapter();
        categoryAdapter.setCategoryLists(categoryLists);
        categoryRecyclerView.setAdapter(categoryAdapter);
    }

    private void setBestSellerRecyclerView(ArrayList<BestSeller_> bestSeller_arrayList){
        bestSellerRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        bestSellerRecyclerView.setHasFixedSize(false);
        bestSellerAdapter = new BestSellerAdapter();
        bestSellerAdapter.setBestSellerArrayList(bestSeller_arrayList);
        bestSellerRecyclerView.setAdapter(bestSellerAdapter);
    }


}
