package smartpan.com.toptato_training.ui.splash;

import android.content.Context;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.slash.TokenResponse;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;
import smartpan.com.toptato_training.utils.AppPreferences;

public class SplashPresenter {
    Context context;

    public SplashPresenter(Context context) {
        this.context = context;

    }

    public void getToken() {
        String username = "TopTato";
        String password = "T0pT@T0.P@$$";
        String grant_type = "password";
        String deviceName = "AndroidTrainer";

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TokenResponse> call = apiInterface.getToken(username, password, grant_type, deviceName);
        call.enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                if (response.isSuccessful()) {
                    String token = response.body().getAccessToken();
                    AppPreferences.open(context).saveToken(token);
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Log.e("error", t.getMessage());
            }
        });
    }

}
