package smartpan.com.toptato_training.ui.fragments.FragmentProducts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import smartpan.com.toptato_training.Network.model.category.CategoryList;
import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.fragments.FragmentHome.HomeFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentProductFilter.ProductFilterFragment;
import smartpan.com.toptato_training.utils.Connectivity;

public class ProductsFragment extends androidx.fragment.app.Fragment implements ProductsListener{

    private ProductsAdapter adapter;
    private ProgressBar progressBar;
    List<CategoryList> itemsList = new ArrayList<>();


    public ProductsFragment(){

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView;
        if (Connectivity.isNetworkAvailable(getActivity())) {     //if true
            rootView = inflater.inflate(R.layout.fragment_products, container, false);
        }
        else {      //if false
            rootView = inflater.inflate(R.layout.no_internet, container, false);
        }

        return rootView;
        //return inflater.inflate(R.layout.fragment_products, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!Connectivity.isNetworkAvailable(getActivity())){
            Button button = view.findViewById(R.id.btn_check);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isNetworkAvailable(getActivity())){
                        getFragmentManager().beginTransaction()
                                .replace(R.id.root_frame, new ProductsFragment()).commit();
                        ViewPager viewPager = Objects.requireNonNull(getActivity()).findViewById(R.id.viewPager);
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
        else {
            progressBar = view.findViewById(R.id.products_progress_bar);
            Toolbar toolbar = view.findViewById(R.id.products_toolBar);
            ImageView toolbarBack = toolbar.findViewById(R.id.products_return_home);
            ProductsPresenter presenter = new ProductsPresenter(getActivity(), this);
            presenter.getCategories();
            initRecyclerView(view);
            toolbarBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ViewPager viewPager = Objects.requireNonNull(getActivity()).findViewById(R.id.viewPager);
                    viewPager.setCurrentItem(0);
                }
            });
        }
    }

    private void initRecyclerView(View view){
        RecyclerView recyclerView = view.findViewById(R.id.products_recycler_view);
        adapter = new ProductsAdapter(getContext());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Map<String,String> parameters = new HashMap<>();
                        CategoryList categoryList = itemsList.get(position);
                        String id = String.valueOf(categoryList.getId());
                        parameters.put("categoryId",id);
                        Bundle bundle = new Bundle();
                        bundle.putString("cat_id",id);
                        bundle.putString("cat_title",categoryList.getEnglishName());
                        ProductFilterFragment productFilterFragment = new ProductFilterFragment();
                        productFilterFragment.setArguments(bundle);
                        FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        trans.replace(R.id.root_frame, productFilterFragment);
                        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        trans.addToBackStack(null);
                        trans.commit();
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }

    @Override
    public void setProductsList(List<CategoryList> categoriesList) {
        adapter.setProductsList(categoriesList);
        this.itemsList = categoriesList;
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }
}

