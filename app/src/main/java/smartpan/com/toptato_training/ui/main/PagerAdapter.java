package smartpan.com.toptato_training.ui.main;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import org.jetbrains.annotations.NotNull;

import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.fragments.FragmentAccount.AccountFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentCart.CartFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentHome.HomeFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentOffers.OffersFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentRoot.AccountRootFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentRoot.CartRootFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentRoot.HomeRootFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentRoot.OffersRootFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentRoot.ProductsRootFragment;

public class PagerAdapter extends FragmentPagerAdapter {
    private int numOfTabs;
    Context context;
    private String tabTitles[] = new String[] { "Home", "Products" ,"Cart","Offers","Account"};
    private int tabImages[] = new int[]{R.drawable.ic_home_1,R.drawable.ic_hanger,R.drawable.ic_handbag,R.drawable.ic_sale,R.drawable.ic_user_};

    public PagerAdapter(FragmentManager fm, int numOfTabs, Context context) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.numOfTabs = numOfTabs;
        this.context = context;
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HomeRootFragment();
            case 1:
                return new ProductsRootFragment();
            case 2:
                return new CartRootFragment();
            case 3:
                return new OffersRootFragment();
            case 4:
                return new AccountRootFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab,null);
        TextView tv = v.findViewById(R.id.custom_text);
        ImageView iv = v.findViewById(R.id.custom_image);
        tv.setTextColor(Color.parseColor("#000000"));
        tv.setText(tabTitles[position]);
        iv.setImageResource(tabImages[position]);
        return v;
    }

}
