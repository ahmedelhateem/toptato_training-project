package smartpan.com.toptato_training.ui.main;

import android.app.Activity;
import android.graphics.Color;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import smartpan.com.toptato_training.R;

public class MainPresenter {

    Activity context;
    MainInterface mainInterface;

    public MainPresenter(Activity context, MainInterface mainInterface) {
        this.context = context;
        this.mainInterface = mainInterface;
    }

    void setTabSettings(TabLayout tabLayout,ViewPager viewPager,PagerAdapter pagerAdapter){

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(pagerAdapter.getTabView(i));
            }
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 1) {
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_hanger_h);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#CAC27F"));
                } else if (tab.getPosition() == 2) {
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_handbag_h);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#CAC27F"));

                }
                else if (tab.getPosition() == 3){
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_sale_h);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#CAC27F"));

                }
                else if (tab.getPosition() == 4){
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_user_h);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#CAC27F"));

                }
                else {
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_home_h);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#CAC27F"));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 1) {
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_hanger);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#000000"));
                } else if (tab.getPosition() == 2) {
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_handbag);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#000000"));

                }
                else if (tab.getPosition() == 3){
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_sale);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#000000"));

                }
                else if (tab.getPosition() == 4){
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_user_);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#000000"));

                }
                else {
                    ImageView iv = tab.getCustomView().findViewById(R.id.custom_image);
                    iv.setImageResource(R.drawable.ic_home_1);
                    TextView tv = tab.getCustomView().findViewById(R.id.custom_text);
                    tv.setTextColor(Color.parseColor("#000000"));
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
