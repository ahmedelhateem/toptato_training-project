package smartpan.com.toptato_training.ui.fragments.FragmentProductFilter;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.FilterdCategories.CategoryFilterResponse;
import smartpan.com.toptato_training.Network.model.FilterdCategories.ItemList;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;
import smartpan.com.toptato_training.utils.AppPreferences;
import smartpan.com.toptato_training.utils.Constants;

public class ProductFilterPresenter {
    Context context;
    ProductFilterListener listener;

    public ProductFilterPresenter(Context context, ProductFilterListener listener) {
        this.context = context;
        this.listener = listener;
    }

    void getItems(String id){

        Map<String,String> parameters = new HashMap<>();
        parameters.put("categoryId",id);
        CategoryFilterResponse categoryFilterResponse = new CategoryFilterResponse();
        categoryFilterResponse.setSuccess("ok");
        categoryFilterResponse.setItemList(null);
        String token = AppPreferences.open(context).getToken();
        String bearerToken = Constants.TOKEN_BEARER + token;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CategoryFilterResponse> call = apiInterface.getFilteredCategories(bearerToken,parameters,categoryFilterResponse);
        call.enqueue(new Callback<CategoryFilterResponse>() {
            @Override
            public void onResponse(Call<CategoryFilterResponse> call, Response<CategoryFilterResponse> response) {
                if (response.isSuccessful()){
                    listener.hideProgressBar();
                    List<ItemList> itemsList = response.body().getItemList();
                    //Log.d("hhh",itemsList.toString());
                    listener.setProductsFilteredList(itemsList);
                    //adapter.setProductsFilteredList(itemsList);
                    //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
                    //recyclerView.setAdapter(adapter);

                }


            }

            @Override
            public void onFailure(Call<CategoryFilterResponse> call, Throwable t) {

            }
        });

    }
}
