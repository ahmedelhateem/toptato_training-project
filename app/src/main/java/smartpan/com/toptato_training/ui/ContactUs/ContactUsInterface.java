package smartpan.com.toptato_training.ui.ContactUs;

import smartpan.com.toptato_training.Network.model.ContactUS.ContactUsResponse;

public interface ContactUsInterface {
    void sendContactUsResponse(ContactUsResponse response);

}
