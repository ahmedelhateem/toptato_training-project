package smartpan.com.toptato_training.ui.fragments.FragmentHome;

import java.util.ArrayList;

import smartpan.com.toptato_training.Network.model.best_offers.BestOffer;
import smartpan.com.toptato_training.Network.model.best_seller.BestSeller_;
import smartpan.com.toptato_training.Network.model.category.CategoryList;

public interface HomeInterface {

    void setImageSlider(ArrayList<String> urls);

    void setBestOffers(ArrayList<BestOffer> bestOffers);

    void setCategories(ArrayList<CategoryList> categories);

    void setBestSeller(ArrayList<BestSeller_> bestSeller);
}
