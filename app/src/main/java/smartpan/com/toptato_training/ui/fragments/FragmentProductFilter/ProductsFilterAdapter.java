package smartpan.com.toptato_training.ui.fragments.FragmentProductFilter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import smartpan.com.toptato_training.Network.model.FilterdCategories.ItemList;
import smartpan.com.toptato_training.R;

public class ProductsFilterAdapter extends RecyclerView.Adapter<ProductsFilterAdapter.ProductFilterViewHolder> {

    Context context;
    List<ItemList> itemsList = new ArrayList<>();

    public ProductsFilterAdapter(Context context) {
        this.context = context;
    }

    public void setProductsFilteredList(List<ItemList> itemsList){
        if (itemsList == null)
            return;
        this.itemsList = itemsList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ProductsFilterAdapter.ProductFilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_filter_row,parent,false);
        return new ProductsFilterAdapter.ProductFilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsFilterAdapter.ProductFilterViewHolder holder, int position) {
        ItemList itemList = itemsList.get(position);
        if (itemList.getIsPromotion()){
            Picasso.get().load(itemList.getCategoryImage()).placeholder(R.drawable.no_pro).into(holder.productFilterImage);
            holder.productFilterTitle.setText(itemList.getEnglishName());
            holder.productFilterRating.setRating(itemList.getReviewStars());
            holder.productFilterPrice.setText(itemList.getPromotionUnitPrice() + " SR");
            holder.productFilterOffer.setVisibility(View.VISIBLE);
            holder.productFilterDeleted.setVisibility(View.VISIBLE);
            holder.productFilterDeleted.setText(itemList.getUnitPrice() + " SR");
        }
        else {
            Picasso.get().load(itemList.getCategoryImage()).placeholder(R.drawable.no_pro).into(holder.productFilterImage);
            holder.productFilterTitle.setText(itemList.getEnglishName());
            holder.productFilterRating.setRating(itemList.getReviewStars());
            holder.productFilterPrice.setText(itemList.getUnitPrice() + " SR");
            holder.productFilterOffer.setVisibility(View.GONE);
            holder.productFilterDeleted.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    class ProductFilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView productFilterImage;
        TextView productFilterOffer;
        TextView productFilterTitle;
        RatingBar productFilterRating;
        TextView productFilterPrice;
        TextView productFilterDeleted;

         ProductFilterViewHolder(@NonNull View itemView) {
            super(itemView);
            productFilterImage = itemView.findViewById(R.id.product_filter_image);
            productFilterOffer = itemView.findViewById(R.id.product_filter_offer);
            productFilterTitle = itemView.findViewById(R.id.product_filter_title);
            productFilterRating = itemView.findViewById(R.id.product_filter_rating);
            productFilterPrice = itemView.findViewById(R.id.product_filter_price);
            productFilterDeleted = itemView.findViewById(R.id.product_filter_price_deleted);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }
}

