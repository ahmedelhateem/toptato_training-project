package smartpan.com.toptato_training.ui.fragments.FragmentProducts;


import java.util.List;

import smartpan.com.toptato_training.Network.model.category.CategoryList;

public interface ProductsListener {
    void setProductsList(List<CategoryList> categoriesList);
    void hideProgressBar();
}
