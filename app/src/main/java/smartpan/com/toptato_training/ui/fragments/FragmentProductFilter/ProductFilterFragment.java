package smartpan.com.toptato_training.ui.fragments.FragmentProductFilter;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.FilterdCategories.CategoryFilterResponse;
import smartpan.com.toptato_training.Network.model.FilterdCategories.ItemList;
import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;
import smartpan.com.toptato_training.utils.AppPreferences;
import smartpan.com.toptato_training.utils.Constants;

public class ProductFilterFragment extends androidx.fragment.app.Fragment implements ProductFilterListener{

    ProgressBar progressBar;
    RecyclerView recyclerView;
    ProductsFilterAdapter adapter;
    ProductFilterPresenter presenter;
    FloatingActionButton fab;
    DrawerLayout drawer;
    RelativeLayout dialog;
    ListView filter;
    public ProductFilterFragment(){

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_products_filter, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = view.findViewById(R.id.products_filter_progress_bar);
        recyclerView = view.findViewById(R.id.products_filter_recycler_view);
        Toolbar toolbar = view.findViewById(R.id.products_filter_toolBar);
        TextView toolbarTitle = toolbar.findViewById(R.id.toolbar_filter_title);
        ImageView toolbarBack = toolbar.findViewById(R.id.products_filter_return_home);
        fab = view.findViewById(R.id.products_filter_fab);
        adapter = new ProductsFilterAdapter(getActivity());
        presenter = new ProductFilterPresenter(getActivity(),this);
        Bundle b = this.getArguments();
        String id = b.getString("cat_id");
        String title = b.getString("cat_title");
        toolbarTitle.setText(title);
        presenter.getItems(id);
        initRecyclerView(view);
        toolbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnHome();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog=new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar);
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                dialog.setContentView(R.layout.filter_dialog);
                Spinner spinner = dialog.findViewById(R.id.filter_spinner);
                //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.spinner_item,elements);
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                        R.array.spinner_entries, R.layout.spinner_item);
                adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                dialog.show();

            }
        });

    }

    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.products_filter_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setProductsFilteredList(List<ItemList> itemsList) {
        adapter.setProductsFilteredList(itemsList);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void returnHome(){
        Objects.requireNonNull(getActivity()).onBackPressed();
    }



}
