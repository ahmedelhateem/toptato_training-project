package smartpan.com.toptato_training.ui.fragments.FragmentProducts;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.category.CategoriesResponse;
import smartpan.com.toptato_training.Network.model.category.CategoryList;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;
import smartpan.com.toptato_training.ui.category.CategoryListener;
import smartpan.com.toptato_training.utils.AppPreferences;
import smartpan.com.toptato_training.utils.Constants;

public class ProductsPresenter {

    Context context;
    ProductsListener productsListener;

    public ProductsPresenter(Context context, ProductsListener productsListener) {
        this.context = context;
        this.productsListener = productsListener;
    }

     void getCategories() {

        // put saved token
        String token = AppPreferences.open(context).getToken();
        String bearerToken = Constants.TOKEN_BEARER + token;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CategoriesResponse> call = apiInterface.getCategories(bearerToken);
        call.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                if (response.isSuccessful()) {
                    productsListener.hideProgressBar();
                    List<CategoryList> categoryList = response.body().getCategoryList();
                    //Log.e("result",categoryList.toString());
                    productsListener.setProductsList(categoryList);


                   /* if (status.equals("ok")) {
                        //Log.e("categories",status);
                    }*/

                } else {

                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {

            }
        });
    }

}
