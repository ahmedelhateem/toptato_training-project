package smartpan.com.toptato_training.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.main.MainActivity;
import smartpan.com.toptato_training.utils.AppPreferences;

public class SplashActivity extends AppCompatActivity {
    private SplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashPresenter = new SplashPresenter(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        splashPresenter.getToken();
        Log.e("token", AppPreferences.open(this).getToken());

        new Handler().postDelayed(() -> {

            startActivity(new Intent(SplashActivity.this, MainActivity.class));

            finish();
        }, 2000);
    }
}
