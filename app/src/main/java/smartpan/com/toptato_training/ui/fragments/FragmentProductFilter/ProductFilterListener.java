package smartpan.com.toptato_training.ui.fragments.FragmentProductFilter;

import java.util.List;

import smartpan.com.toptato_training.Network.model.FilterdCategories.ItemList;

public interface ProductFilterListener {
    void setProductsFilteredList(List<ItemList> itemsList);
    void hideProgressBar();
}
