package smartpan.com.toptato_training.ui.fragments.FragmentAccount;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import java.util.Objects;

import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.ContactUs.ContactUs;
import smartpan.com.toptato_training.ui.SettingActivity.SettingActivity;
import smartpan.com.toptato_training.ui.fragments.FragmentOffers.OffersFragment;
import smartpan.com.toptato_training.ui.splash.SplashActivity;
import smartpan.com.toptato_training.utils.Connectivity;


public class AccountFragment extends androidx.fragment.app.Fragment {

    public AccountFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView;
        if (Connectivity.isNetworkAvailable(getActivity())) {     //if true
            rootView = inflater.inflate(R.layout.fragment_account, container, false);
        }
        else {      //if false
            rootView = inflater.inflate(R.layout.no_internet, container, false);
        }

        return rootView;
        //return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!Connectivity.isNetworkAvailable(getActivity())){
            Button button = view.findViewById(R.id.btn_check);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isNetworkAvailable(getActivity())){
                        getFragmentManager().beginTransaction()
                                .replace(R.id.account_root_frame, new AccountFragment()).commit();
                        ViewPager viewPager = Objects.requireNonNull(getActivity()).findViewById(R.id.viewPager);
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
        else {
            Toolbar toolbar = view.findViewById(R.id.account_toolBar);
            ImageButton imageButton = toolbar.findViewById(R.id.account_return_home);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ViewPager viewPager = Objects.requireNonNull(getActivity()).findViewById(R.id.viewPager);
                    viewPager.setCurrentItem(0);
                }
            });
            CardView Setting= view.findViewById(R.id.card_settings);
            CardView ContactUS=view.findViewById(R.id.card_contact_us);
            Setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), SettingActivity.class));
                }
            });
            ContactUS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), ContactUs.class));
                }
            });
        }
    }
}
