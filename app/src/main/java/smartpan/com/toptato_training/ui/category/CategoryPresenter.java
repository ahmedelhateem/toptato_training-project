package smartpan.com.toptato_training.ui.category;

import android.content.Context;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.category.CategoriesResponse;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;
import smartpan.com.toptato_training.utils.AppPreferences;

public class CategoryPresenter {

    Context context;
    CategoryListener categoryListener;

    public CategoryPresenter(Context context, CategoryListener categoryListener) {
        this.context = context;
        this.categoryListener = categoryListener;
    }

    public void getCategories() {

        // put saved token
        String token = AppPreferences.open(context).getToken();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CategoriesResponse> call = apiInterface.getCategories(token);
        call.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                if (response.isSuccessful()) {
                    String status = response.body().getSuccess();
                    if (status.equals("ok")) {
                        Log.e("categories",status);
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {

            }
        });
    }

}
