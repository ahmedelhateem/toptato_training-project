package smartpan.com.toptato_training.ui.fragments.FragmentOffers;

import android.content.Context;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.FilterdCategories.CategoryFilterResponse;
import smartpan.com.toptato_training.Network.model.category.CategoriesResponse;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;
import smartpan.com.toptato_training.utils.AppPreferences;
import smartpan.com.toptato_training.utils.Constants;

public class OffersPresenter {
    Context context;
    OffersListener listener;

    public OffersPresenter(Context context, OffersListener listener) {
        this.context = context;
        this.listener = listener;
    }

    void getOffers(){
        String token = AppPreferences.open(context).getToken();
        String bearerToken = Constants.TOKEN_BEARER + token;
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        CategoryFilterResponse categoryFilterResponse = new CategoryFilterResponse();
        categoryFilterResponse.setSuccess("ok");
        categoryFilterResponse.setItemList(null);
        Call<CategoryFilterResponse> call = apiInterface.getOffers(bearerToken,categoryFilterResponse);
        call.enqueue(new Callback<CategoryFilterResponse>() {
            @Override
            public void onResponse(Call<CategoryFilterResponse> call, Response<CategoryFilterResponse> response) {
                listener.hideProgressBar();
                listener.setOffersList(response.body().getItemList());
            }

            @Override
            public void onFailure(Call<CategoryFilterResponse> call, Throwable t) {

            }
        });
    }
}
