package smartpan.com.toptato_training.ui.ContactUs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Objects;

import smartpan.com.toptato_training.Network.model.ContactUS.ContactUsResponse;
import smartpan.com.toptato_training.R;

import static android.app.PendingIntent.getActivity;

public class ContactUs extends AppCompatActivity implements ContactUsInterface{
EditText name,message,email;
Button sendButton;
contactUsPresenter  contactUsPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
       name =findViewById(R.id.nameText);
       message =findViewById(R.id.messageText);
       email=findViewById(R.id.emailText);
        sendButton =findViewById(R.id.btnSend);
        contactUsPresenter =new contactUsPresenter(this,this);
        ////validation
        String email1 = email.getText().toString();

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
/*
* if(email.getText().toString().isEmpty()) {
               Toast.makeText(getApplicationContext(),"enter email address",Toast.LENGTH_SHORT).show();
            }else {
               if (email.getText().toString().trim().matches(emailPattern)) {
                  Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
               } else {
                  Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
               }
            }
         }
      });*/


            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(email.getText().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(),"enter email address",Toast.LENGTH_SHORT).show();
                    }else {
                        if (email.getText().toString().trim().matches(emailPattern)) {
                            Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
                            contactUsPresenter.contactUs(name.getText().toString(),email1,message.getText().toString());
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            });







        Toolbar toolbar = findViewById(R.id.contactUs_toolBar);
        ImageButton imageButton = toolbar.findViewById(R.id.ib_contactUs_return_home);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });


    }


    @Override
    public void sendContactUsResponse(ContactUsResponse response) {
        if (  response.getSuccess().equals("Ok")){
            Toast.makeText(this,response.getArabicMessage(),Toast.LENGTH_LONG).show();

            // startActivity(new Intent(ContactUs.this, AccountFragment.class));
        }
        else{
            Toast.makeText(this,"failed",Toast.LENGTH_LONG).show();
        }

    }
}
