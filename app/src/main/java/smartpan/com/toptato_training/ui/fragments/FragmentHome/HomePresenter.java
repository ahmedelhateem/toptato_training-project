package smartpan.com.toptato_training.ui.fragments.FragmentHome;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.best_offers.BestOffer;
import smartpan.com.toptato_training.Network.model.best_offers.BestOffersResponse;
import smartpan.com.toptato_training.Network.model.best_seller.BestSeller;
import smartpan.com.toptato_training.Network.model.best_seller.BestSeller_;
import smartpan.com.toptato_training.Network.model.category.CategoriesResponse;
import smartpan.com.toptato_training.Network.model.category.CategoryList;
import smartpan.com.toptato_training.Network.model.slider.SiteSlider;
import smartpan.com.toptato_training.Network.model.slider.SiteSliderList;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;
import smartpan.com.toptato_training.utils.AppPreferences;
import smartpan.com.toptato_training.utils.Constants;

public class HomePresenter {
    private Context context;
    private HomeInterface homeInterface;
    String token;
    ApiInterface apiInterface;


    public HomePresenter(Context context, HomeInterface homeInterface) {
        this.context = context;
        this.homeInterface = homeInterface;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

    }

    public void getSliderImages(){
        ArrayList<String> urls = new ArrayList<>();

        token = AppPreferences.open(context).getToken();
        Call<SiteSlider> siteSliderCall = apiInterface.getSliderImages(Constants.TOKEN_BEARER + token);

        siteSliderCall.enqueue(new Callback<SiteSlider>() {
            @Override
            public void onResponse(Call<SiteSlider> call, Response<SiteSlider> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        SiteSlider siteSlider = response.body();
                        if (siteSlider.getSuccess().equals("ok")){
                            for (SiteSliderList siteSliderList: siteSlider.getSiteSliderList()) {
                                urls.add(siteSliderList.getStoryImage());
                            }
                            homeInterface.setImageSlider(urls);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SiteSlider> call, Throwable t) {
                Log.d("Failure in request", t.getMessage());
            }
        });
    }

    public void getBestOffers(){
        ArrayList<BestOffer> bestOffersResponses = new ArrayList<>();

        Call<BestOffersResponse> bestOffersResponseCall = apiInterface.getBestOffers(Constants.TOKEN_BEARER + token);
        bestOffersResponseCall.enqueue(new Callback<BestOffersResponse>() {
            @Override
            public void onResponse(Call<BestOffersResponse> call, Response<BestOffersResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        BestOffersResponse bestOffersResponse = response.body();
                        if (bestOffersResponse.getSuccess().equals("ok")){
                            bestOffersResponses.addAll(bestOffersResponse.getBestOffers());
                            homeInterface.setBestOffers(bestOffersResponses);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BestOffersResponse> call, Throwable t) {
                Log.d("Failure in request", t.getMessage());
            }
        });
    }

    public void getCategories(){
        ArrayList<CategoryList> categoryLists = new ArrayList<>();

        Call<CategoriesResponse> categoriesResponseCall = apiInterface.getCategories(Constants.TOKEN_BEARER + token);

        categoriesResponseCall.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        CategoriesResponse categoriesResponse = response.body();
                        if (categoriesResponse.getSuccess().equals("ok")){
                            categoryLists.addAll(categoriesResponse.getCategoryList());
                            homeInterface.setCategories(categoryLists);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                Log.d("Failure in request", t.getMessage());
            }
        });
    }

    public void getBestSeller(){
        ArrayList<BestSeller_> bestSellerArrayList = new ArrayList<>();

        Call<BestSeller> bestSellerCall = apiInterface.getBestSeller(Constants.TOKEN_BEARER + token);

        bestSellerCall.enqueue(new Callback<BestSeller>() {
            @Override
            public void onResponse(Call<BestSeller> call, Response<BestSeller> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        BestSeller bestSeller = response.body();
                        if (bestSeller.getSuccess().equals("ok")){
                            bestSellerArrayList.addAll(bestSeller.getBestSeller());
                            homeInterface.setBestSeller(bestSellerArrayList);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BestSeller> call, Throwable t) {
                Log.d("Failure in request", t.getMessage());
            }
        });
    }
}
