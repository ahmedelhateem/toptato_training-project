package smartpan.com.toptato_training.ui.Registration;

import android.content.Context;
import android.util.Log;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.Registration.RegistrationResponse;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;

public class RegistrationPresenter {

    private static final String token="0SnJlVR6emMqdqB6o2evvdMtZ5whyVEc9ukH5Box-W7Yk8XuPfJJznhrUwqWto7nfgWQ90qZN6tntX4L9gRZ2Vqz9cS2t5tZWxWDyZMs_R3e3YdGiynGiGULw_XjSTZoDOog4zr8je8ZylPxKV0yl_1hvFuy1Ie22ARS2x9Xz4d9MSeillyfXNEZB0Qh3qgV53vWxisBhyJiQ1521wvUW_mOVwocjA_pMd_cqjJbS2FaaIEWJ1VU_k7RXF-AwNC6";

    Context context;
    RegistrationListener listener;
    public RegistrationPresenter(Context context,RegistrationListener listener){
        this.listener=listener;
        this.context=context;

    }
    public void getResponse(Map<String,String> map){
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RegistrationResponse> call = apiInterface.register("Bearer "+token,map);
        call.enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                listener.check(response.body().getSuccess());
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                Log.e("error", t.getMessage());
            }
        });




    }
}