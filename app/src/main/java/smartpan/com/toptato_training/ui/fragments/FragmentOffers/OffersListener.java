package smartpan.com.toptato_training.ui.fragments.FragmentOffers;

import java.util.List;

import smartpan.com.toptato_training.Network.model.FilterdCategories.ItemList;

public interface OffersListener {
    void setOffersList(List<ItemList> itemsList);
    void hideProgressBar();
}
