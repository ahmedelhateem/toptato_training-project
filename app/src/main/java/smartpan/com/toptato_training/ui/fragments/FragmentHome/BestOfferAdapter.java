package smartpan.com.toptato_training.ui.fragments.FragmentHome;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import smartpan.com.toptato_training.Network.model.best_offers.BestOffer;
import smartpan.com.toptato_training.R;

public class BestOfferAdapter extends RecyclerView.Adapter<BestOfferAdapter.BestOfferViewHolder>{

    private ArrayList<BestOffer> bestOffers;

    public void setBestOffers(ArrayList<BestOffer> bestOffers) {
        this.bestOffers = bestOffers;
    }

    @NonNull
    @Override
    public BestOfferViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.best_offer_item, null);
        return new BestOfferViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BestOfferViewHolder holder, int position) {
        if (Locale.getDefault().getDisplayLanguage().equals("English")){
            holder.tv_title_best_offer.setText(bestOffers.get(position).getEnglishName());
            holder.tv_sale_price_best_offer.setText(String.valueOf(bestOffers.get(position).getPromotionUnitPrice() + " S.R."));
            holder.tv_original_price_best_offer.setText(String.valueOf(bestOffers.get(position).getUnitPrice()));
            holder.tv_original_price_best_offer.setPaintFlags(holder.tv_original_price_best_offer.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            Picasso.get().load(bestOffers.get(position).getCategoryImage()).fit().into(holder.iv_best_offer);
            Picasso.get().load(R.drawable.offer_e).into(holder.iv_sale_banner_best_offer);
        }
        else {
            holder.tv_title_best_offer.setText(bestOffers.get(position).getArabicName());
            holder.tv_sale_price_best_offer.setText(String.valueOf(bestOffers.get(position).getPromotionUnitPrice() + " S.R."));
            holder.tv_original_price_best_offer.setText(String.valueOf(bestOffers.get(position).getUnitPrice()));
            holder.tv_original_price_best_offer.setPaintFlags(holder.tv_original_price_best_offer.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            Picasso.get().load(bestOffers.get(position).getCategoryImage()).fit().into(holder.iv_best_offer);
            Picasso.get().load(R.drawable.offer).fit().into(holder.iv_sale_banner_best_offer);
        }
    }

    @Override
    public int getItemCount() {
        return bestOffers.size();
    }

    public class BestOfferViewHolder extends RecyclerView.ViewHolder{

        public TextView tv_title_best_offer;
        public TextView tv_sale_price_best_offer;
        public TextView tv_original_price_best_offer;
        public ImageView iv_best_offer;
        public ImageView iv_sale_banner_best_offer;

        public BestOfferViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_title_best_offer = itemView.findViewById(R.id.tv_title_best_offer);
            tv_sale_price_best_offer = itemView.findViewById(R.id.tv_sale_price_best_offer);
            tv_original_price_best_offer = itemView.findViewById(R.id.tv_original_price_best_offer);
            iv_best_offer = itemView.findViewById(R.id.iv_best_offer);
            iv_sale_banner_best_offer = itemView.findViewById(R.id.iv_sale_banner_best_offer);
        }
    }
}
