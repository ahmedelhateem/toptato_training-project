package smartpan.com.toptato_training.ui.fragments.FragmentRoot;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.fragments.FragmentCart.CartFragment;

public class CartRootFragment extends Fragment {
    private static final String TAG = "CartRootFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.cart_root_fragment, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        /*
         * When this container fragment is created, we fill it with our first
         * "real" fragment
         */
        transaction.replace(R.id.cart_root_frame, new CartFragment());

        transaction.commit();

        return view;
    }
}

