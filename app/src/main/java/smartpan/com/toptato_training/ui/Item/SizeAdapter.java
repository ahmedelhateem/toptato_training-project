package smartpan.com.toptato_training.ui.Item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import smartpan.com.toptato_training.Network.model.ItemDetails.Color;
import smartpan.com.toptato_training.Network.model.ItemDetails.MatrixList;
import smartpan.com.toptato_training.R;

public abstract class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.ViewHolder> {
    List<MatrixList> size;
    Context context;
    public SizeAdapter(ArrayList<MatrixList>size, Context context){
        this.size=size;
        this.context=context;
    }
    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.shape_size,parent,false);
        ViewHolder viewHolder=new ViewHolder(v);
    return viewHolder;}

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.size.setText(size.get(position).getSizeEnglishName());

        holder.size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.size.setText(size.get(position).getSizeEnglishName());
                if (holder.size.isChecked()){
                    getcolor(size.get(position).getColors(),position);


                    notifyDataSetChanged();


                }
                else {
                    holder.size.setChecked(true);
                    holder.size.setText(size.get(position).getSizeEnglishName());

                }


            }
        });

    }

    protected abstract void getcolor(List<Color> colors,int position);

    @Override
    public int getItemCount() {
        return size.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ToggleButton size;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            size=itemView.findViewById(R.id.size);
        }
    }
}
