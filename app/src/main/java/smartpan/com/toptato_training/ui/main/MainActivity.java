package smartpan.com.toptato_training.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import smartpan.com.toptato_training.R;

public class MainActivity extends AppCompatActivity implements MainInterface{

    TabLayout tabLayout;
    ViewPager viewPager;
    PagerAdapter pagerAdapter;
    TabItem tabHome;
    TabItem tabProducts;
    TabItem tabCart;
    TabItem tabOffers;
    TabItem tabAccount;
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout = findViewById(R.id.tablayout);
        tabHome = findViewById(R.id.tab_Home);
        tabProducts = findViewById(R.id.tab_products);
        tabCart = findViewById(R.id.tab_cart);
        tabOffers = findViewById(R.id.tab_offers);
        tabAccount = findViewById(R.id.tab_account);
        viewPager = findViewById(R.id.viewPager);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(),this);
        presenter = new MainPresenter(this,this);
        viewPager.setAdapter(pagerAdapter);
        presenter.setTabSettings(tabLayout,viewPager,pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }



}
