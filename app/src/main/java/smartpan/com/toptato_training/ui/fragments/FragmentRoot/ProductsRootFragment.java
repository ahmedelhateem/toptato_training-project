package smartpan.com.toptato_training.ui.fragments.FragmentRoot;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.fragments.FragmentProducts.ProductsFragment;

public class ProductsRootFragment extends Fragment {
    private static final String TAG = "ProductsRootFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.products_root_fragment, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        /*
         * When this container fragment is created, we fill it with our first
         * "real" fragment
         */
        transaction.replace(R.id.root_frame, new ProductsFragment());

        transaction.commit();

        return view;
    }
}
