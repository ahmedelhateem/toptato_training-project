package smartpan.com.toptato_training.ui.fragments.FragmentHome;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import smartpan.com.toptato_training.Network.model.category.CategoryList;
import smartpan.com.toptato_training.R;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private ArrayList<CategoryList> categoryLists;

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_item, null);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        if (Locale.getDefault().getDisplayLanguage().equals("English")) {
            holder.tv_categories_title.setText(categoryLists.get(position).getEnglishName());
        } else {
            holder.tv_categories_title.setText(categoryLists.get(position).getArabicName());
        }
        Picasso.get()
                .load(categoryLists.get(position).getCategoryImage())
                .fit()
                .into(holder.iv_category);
    }

    @Override
    public int getItemCount() {
        return categoryLists != null ? categoryLists.size() : 0;
    }

    public void setCategoryLists(ArrayList<CategoryList> categoryLists) {
        this.categoryLists = categoryLists;
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_categories_title;
        public ImageView iv_category;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_category = itemView.findViewById(R.id.iv_categories);
            tv_categories_title = itemView.findViewById(R.id.tv_categories_title);
        }
    }
}
