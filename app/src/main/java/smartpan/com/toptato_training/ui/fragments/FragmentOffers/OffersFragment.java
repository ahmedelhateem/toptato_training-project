package smartpan.com.toptato_training.ui.fragments.FragmentOffers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import java.util.List;
import java.util.Objects;

import smartpan.com.toptato_training.Network.model.FilterdCategories.ItemList;
import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.fragments.FragmentCart.CartFragment;
import smartpan.com.toptato_training.ui.fragments.FragmentProductFilter.ProductsFilterAdapter;
import smartpan.com.toptato_training.utils.Connectivity;

public class OffersFragment extends androidx.fragment.app.Fragment implements OffersListener{

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    OffersPresenter presenter;
    ProductsFilterAdapter adapter;

    public OffersFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView;
        if (Connectivity.isNetworkAvailable(getActivity())) {     //if true
            rootView = inflater.inflate(R.layout.fragment_offers, container, false);
        }
        else {      //if false
            rootView = inflater.inflate(R.layout.no_internet, container, false);
        }

        return rootView;
        //return inflater.inflate(R.layout.fragment_offers, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!Connectivity.isNetworkAvailable(getActivity())){
            Button button = view.findViewById(R.id.btn_check);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isNetworkAvailable(getActivity())){
                        getFragmentManager().beginTransaction()
                                .replace(R.id.offers_root_frame, new OffersFragment()).commit();
                        ViewPager viewPager = Objects.requireNonNull(getActivity()).findViewById(R.id.viewPager);
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
        else {
            Toolbar toolbar = view.findViewById(R.id.offers_toolBar);
            ImageView toolbarBack = toolbar.findViewById(R.id.offers_return_home);
            progressBar = view.findViewById(R.id.offers_progress_bar);
            adapter = new ProductsFilterAdapter(getActivity());
            initRecyclerView(view);
            presenter = new OffersPresenter(getActivity(),this);
            presenter.getOffers();
            toolbarBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ViewPager viewPager = Objects.requireNonNull(getActivity()).findViewById(R.id.viewPager);
                    viewPager.setCurrentItem(0);
                }
            });
        }

    }

    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.offers_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setOffersList(List<ItemList> itemsList) {
        adapter.setProductsFilteredList(itemsList);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }
}
