package smartpan.com.toptato_training.ui.ContactUs;

import android.content.Context;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.ContactUS.ContactUsResponse;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;
import smartpan.com.toptato_training.utils.AppPreferences;
import smartpan.com.toptato_training.utils.Constants;

public class contactUsPresenter {
    ContactUsInterface contactUsInterface;
    Context context;
    public contactUsPresenter(ContactUsInterface  contactUsInterface, Context context){

    this.contactUsInterface =contactUsInterface;
    this.context = context;
    }

    public void contactUs( String name,String email,String message){

        String token= Constants.TOKEN_BEARER+AppPreferences.open(context).getToken();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ContactUsResponse> call = apiInterface.contactUs(token, name, email, message);
        call.enqueue(new Callback<ContactUsResponse>() {
            @Override
            public void onResponse(Call<ContactUsResponse> call, Response<ContactUsResponse> response) {
                if (response.isSuccessful()) {
                    contactUsInterface.sendContactUsResponse(response.body());




                }
            }

            @Override
            public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                Log.e("error", t.getMessage());
            }
        });
    }
}
