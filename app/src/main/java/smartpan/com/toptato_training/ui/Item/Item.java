package smartpan.com.toptato_training.ui.Item;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import smartpan.com.toptato_training.Network.model.ItemDetails.Color;
import smartpan.com.toptato_training.Network.model.ItemDetails.ItemResponse;
import smartpan.com.toptato_training.Network.model.ItemDetails.MatrixList;
import smartpan.com.toptato_training.R;

public class Item extends AppCompatActivity implements ItemLisenter {
    List<MatrixList>details=new ArrayList<>();
    RecyclerView sizeRcycler;
    SizeAdapter sizeAdapter;
    RecyclerView colorRcycler;
    ColorAdapter colorAdapter;
    RecyclerView imageRcycler;
    ImageAdapter imageAdapter;
    ImageView imageView;
    TextView itemNumber,available,desc;
    Button price;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        ItemPresenter itemPresenter=new ItemPresenter(this,this);
        itemPresenter.getItemDetails();
        sizeRcycler=findViewById(R.id.sizes);
        colorRcycler=findViewById(R.id.colors);
        imageRcycler=findViewById(R.id.imageRecycler);
        imageView=findViewById(R.id.image1);
        itemNumber=findViewById(R.id.itemNumber);
        available=findViewById(R.id.available);
        desc=findViewById(R.id.description);
        price=findViewById(R.id.price);


    }

    @Override
    public void details(ItemResponse itemResponse) {
        details=itemResponse.getItemList().get(0).getMatrixList();
        sizeRcycler.setHasFixedSize(true);

        LinearLayoutManager gridLayoutManager=new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);
        sizeRcycler.setLayoutManager(gridLayoutManager);
        colorRcycler.setHasFixedSize(true);
        LinearLayoutManager gridLayoutManagerr=new LinearLayoutManager(Item.this, LinearLayoutManager.HORIZONTAL,false);

        sizeAdapter = new SizeAdapter((ArrayList<MatrixList>) details, this) {
            @Override
            protected void getcolor(List<Color> colors,int position) {



                for (int x = sizeRcycler.getChildCount(), i = 0; i < x; ++i) {
                    if (i!= position) {
                        ViewHolder holder = (ViewHolder) sizeRcycler.getChildViewHolder(sizeRcycler.getChildAt(i));
                        holder.size.setChecked(false);
                    }

                }



                colorRcycler.setLayoutManager(gridLayoutManagerr);
                colorAdapter =new ColorAdapter(colors,Item.this);
                colorAdapter.notifyDataSetChanged();

                colorRcycler.setAdapter(colorAdapter);

            }
        };
        sizeRcycler.setAdapter(sizeAdapter);
        /////////
        imageRcycler.setHasFixedSize(true);
        LinearLayoutManager gridLayoutManage=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        imageRcycler.setLayoutManager(gridLayoutManage);
        imageAdapter=new ImageAdapter(itemResponse.getItemList().get(0).getImagesList(),this);
        imageRcycler.setAdapter(imageAdapter);
        Picasso.get().load(itemResponse.getItemList().get(0).getCategoryImage()).into(imageView);
        itemNumber.setText("itemNumber:  "+itemResponse.getItemList().get(0).getItemCode()+"");
        if(itemResponse.getItemList().get(0).isIsActive()){
            available.setText("Availlable");}
            desc.setText("Description:-\n"+itemResponse.getItemList().get(0).getDescAR());
        price.setText(itemResponse.getItemList().get(0).getUnitPrice()+"");







    }
}
