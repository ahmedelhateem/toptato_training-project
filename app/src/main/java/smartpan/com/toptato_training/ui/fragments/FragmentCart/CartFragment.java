package smartpan.com.toptato_training.ui.fragments.FragmentCart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import java.util.Objects;

import smartpan.com.toptato_training.R;
import smartpan.com.toptato_training.ui.fragments.FragmentProducts.ProductsFragment;
import smartpan.com.toptato_training.utils.Connectivity;

public class CartFragment extends androidx.fragment.app.Fragment {

    public CartFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView;
        if (Connectivity.isNetworkAvailable(getActivity())) {     //if true
            rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        }
        else {      //if false
            rootView = inflater.inflate(R.layout.no_internet, container, false);
        }

        return rootView;
        //return inflater.inflate(R.layout.fragment_cart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!Connectivity.isNetworkAvailable(getActivity())){
            Button button = view.findViewById(R.id.btn_check);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Connectivity.isNetworkAvailable(getActivity())){
                        getFragmentManager().beginTransaction()
                                .replace(R.id.cart_root_frame, new CartFragment()).commit();
                        ViewPager viewPager = Objects.requireNonNull(getActivity()).findViewById(R.id.viewPager);
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
        else{
            //write your code here
        }
    }
}
