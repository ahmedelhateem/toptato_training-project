package smartpan.com.toptato_training.ui.Item;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import smartpan.com.toptato_training.Network.model.ItemDetails.ItemList;
import smartpan.com.toptato_training.Network.model.ItemDetails.ItemResponse;
import smartpan.com.toptato_training.Network.model.slash.TokenResponse;
import smartpan.com.toptato_training.Retrofit.ApiClient;
import smartpan.com.toptato_training.Retrofit.ApiInterface;

public class ItemPresenter {
    Context context;
    ItemLisenter itemLisenter;
    List<ItemList>itemLists=new ArrayList<>();
    private static final String token="l8kes-VdYHYjYHYhEqzZeaYD-ggL2bWkO7MY0PudCTTGLV_Ml6ZTJ2h2ekyyRoga26h0aruXyNtMVAy_h8IHONEou5FQ9K3oJN-3sMdevzlRglcM8KSk51K6L4tiTbcRE2jzbc0z8NEvOsFyGvc3LFD4MUcpSrSJysd4SVObsTcOnz24INzx1BISTL6YB_YxDhlNfLZSpEG9X58nwOjuf8igV3VIyVQWWEdBW2HfBu_S9qgNOH-SehhRFh3su9TG";
    public ItemPresenter(Context context,ItemLisenter itemLisenter){
        this.context=context;
        this.itemLisenter=itemLisenter;



    }
    public void getItemDetails(){
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ItemResponse> call = apiInterface.getitem("Bearer "+token,9416);
        call.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(Call<ItemResponse> call, Response<ItemResponse> response) {

                Toast.makeText(context, response.body().getItemList().get(0).getEnglishName(), Toast.LENGTH_SHORT).show();
                itemLisenter.details(response.body());
            }

            @Override
            public void onFailure(Call<ItemResponse> call, Throwable t) {

            }
        });

    }

}
