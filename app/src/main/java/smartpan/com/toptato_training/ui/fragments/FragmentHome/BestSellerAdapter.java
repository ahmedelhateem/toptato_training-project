package smartpan.com.toptato_training.ui.fragments.FragmentHome;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import smartpan.com.toptato_training.Network.model.best_seller.BestSeller_;
import smartpan.com.toptato_training.R;

public class BestSellerAdapter extends RecyclerView.Adapter<BestSellerAdapter.BestSellerViewHolder> {

    private ArrayList<BestSeller_> bestSellerArrayList;

    @NonNull
    @Override
    public BestSellerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.best_seller_item, null);
        return new BestSellerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BestSellerViewHolder holder, int position) {
        if (Locale.getDefault().getDisplayLanguage().equals("English")) {
            holder.tv_title_best_seller.setText(bestSellerArrayList.get(position).getEnglishName());
            holder.tv_price_best_seller.setText(String.valueOf(bestSellerArrayList.get(position).getUnitPrice() + " S.R."));
            Picasso.get()
                    .load(bestSellerArrayList.get(position).getCategoryImage())
                    .fit()
                    .into(holder.iv_best_seller);
        } else {
            holder.tv_title_best_seller.setText(bestSellerArrayList.get(position).getArabicName());
            holder.tv_price_best_seller.setText(String.valueOf(bestSellerArrayList.get(position).getUnitPrice()));
            Picasso.get()
                    .load(bestSellerArrayList.get(position).getCategoryImage())
                    .fit()
                    .into(holder.iv_best_seller);
        }

        if (bestSellerArrayList.get(position).isIsPromotion()){
            if (Locale.getDefault().getDisplayLanguage().equals("English")){
                holder.tv_original_price_best_seller.setText(String.valueOf(bestSellerArrayList.get(position).getUnitPrice() + " S.R."));
                holder.tv_original_price_best_seller.setPaintFlags(holder.tv_original_price_best_seller.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                Picasso.get()
                        .load(R.drawable.offer_e)
                        .into(holder.iv_sale_banner_best_seller);
            }
            else {
                holder.tv_original_price_best_seller.setText(String.valueOf(bestSellerArrayList.get(position).getUnitPrice() + " S.R."));
                holder.tv_original_price_best_seller.setPaintFlags(holder.tv_original_price_best_seller.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                Picasso.get()
                        .load(R.drawable.offer)
                        .into(holder.iv_sale_banner_best_seller);
            }
        }
    }

    public void setBestSellerArrayList(ArrayList<BestSeller_> bestSellerArrayList) {
        this.bestSellerArrayList = bestSellerArrayList;
    }

    @Override
    public int getItemCount() {
        return bestSellerArrayList != null ? bestSellerArrayList.size() : 0;
    }

    class BestSellerViewHolder extends RecyclerView.ViewHolder {

        public ImageView iv_best_seller;
        public ImageView iv_sale_banner_best_seller;
        public TextView tv_title_best_seller;
        public TextView tv_price_best_seller;
        public TextView tv_original_price_best_seller;

        public BestSellerViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_best_seller = itemView.findViewById(R.id.iv_best_seller);
            iv_sale_banner_best_seller = itemView.findViewById(R.id.iv_sale_banner_best_seller);
            tv_title_best_seller = itemView.findViewById(R.id.tv_title_best_seller);
            tv_price_best_seller = itemView.findViewById(R.id.tv_price_best_seller);
            tv_original_price_best_seller = itemView.findViewById(R.id.tv_original_price_best_seller);
        }
    }
}
