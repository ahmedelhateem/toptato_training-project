package smartpan.com.toptato_training.utils;

public class Constants {
    public final static String Method_GetCategories = "TrainingAPI/GetCategories";
    public static final String Method_GetFilterItems = "TrainingAPI/FilterItems";
    public static final String TOKEN_BEARER = "Bearer ";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public final static String Method_GetToken = "Token";
    public final static String Method_GetItemDetails = "TrainingAPI/GetItemDetails";
    public final static String Method_Register = "TrainingAPI/Register";
    public final static String Method_SiteSlider = "TrainingAPI/GetSiteSlider";
    public final static String Method_BestOffers = "TrainingAPI/GetBestOffers";
    public final static String Method_BestSeller = "TrainingAPI/GetBestSeller";
    public final static String Method_SendContactMessage = "TrainingAPI/SendContactMessage?Name=&Email=&Message=";
    public final static String Method_GetOffers = "TrainingAPI/GetOffers";
}
