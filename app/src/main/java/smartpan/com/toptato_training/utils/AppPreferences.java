package smartpan.com.toptato_training.utils;

import android.content.Context;

import java.util.List;

import smartpan.com.toptato_training.Network.model.FilterdCategories.ItemList;


public class AppPreferences {

    public static final String TOKEN = "token";


    private static net.grandcentrix.tray.AppPreferences pref;
    private static AppPreferences appPreferences;

    public static synchronized AppPreferences open(Context context) {
        if (pref == null) {
            pref = new net.grandcentrix.tray.AppPreferences(context);
        }
        if (appPreferences == null) {
            appPreferences = new AppPreferences();
        }
        return appPreferences;
    }

    public void clearPref() {
        pref.clear();
    }


    public String getToken() {
        return pref.getString(TOKEN, "");
    }


    public void saveToken(String token) {

        pref.put(TOKEN, token);
    }



}
