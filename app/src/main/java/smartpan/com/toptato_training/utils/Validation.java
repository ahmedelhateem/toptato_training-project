package smartpan.com.toptato_training.utils;


import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    public boolean validate(String first,String last,String email,String date,String pass,String gender,Context context){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(pass);


        if(first.isEmpty()||last.isEmpty()||date.isEmpty()||gender.isEmpty()){
            Toast.makeText(context, "please complete your data", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!(!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
            Toast.makeText(context, "email invalid", Toast.LENGTH_SHORT).show();
            return false;

        }
        if(pass.length()<8&&!matcher.matches()){
            Toast.makeText(context, "password invalid", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }
}
